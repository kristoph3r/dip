#include "bgc.h"
#include <math.h>
#include <algorithm>
#include <functional>

Bgc::Bgc(QWidget *parent) : QWidget(parent) //Brightness, Gamma, Contrast
{
    okButton = new QPushButton("OK", this);
    connect(okButton, SIGNAL(clicked()), this, SLOT(close()));

    cancelButton = new QPushButton("Cancel", this);
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));

    resetButton = new QPushButton("Reset", this);
    connect(resetButton, SIGNAL(clicked()), this, SLOT(reset()));

    brightnessSlider = new QSlider(Qt::Horizontal, this);
    brightnessSlider->setRange(-255,255);
    brightnessSlider->setMinimumWidth(180);
    connect(brightnessSlider, SIGNAL(valueChanged(int)), this, SLOT(correctionSlot()));

    brightnessSpinBox = new QSpinBox();
    brightnessSpinBox->setRange(-255,255);
    connect(brightnessSlider, SIGNAL(valueChanged(int)), brightnessSpinBox, SLOT(setValue(int)));
    connect(brightnessSpinBox, SIGNAL(valueChanged(int)), brightnessSlider, SLOT(setValue(int)));

    contrastSlider = new QSlider(Qt::Horizontal, this);
    contrastSlider->setRange(-128,127);
    connect(contrastSlider, SIGNAL(valueChanged(int)), this, SLOT(correctionSlot()));

    contrastSpinBox = new QSpinBox();
    contrastSpinBox->setRange(-128,127);
    connect(contrastSlider, SIGNAL(valueChanged(int)), contrastSpinBox, SLOT(setValue(int)));
    connect(contrastSpinBox, SIGNAL(valueChanged(int)), contrastSlider, SLOT(setValue(int)));

    gammaSlider = new QSlider(Qt::Horizontal, this);
    gammaSlider->setRange(0,19);
    gammaSlider->setValue(10);
    connect(gammaSlider, SIGNAL(valueChanged(int)), this, SLOT(correctionSlot()));

    gammaSpinBox = new QDoubleSpinBox();
    gammaSpinBox->setRange(0,19);
    gammaSpinBox->setValue(1.0);
    connect(gammaSlider, SIGNAL(valueChanged(int)), this, SLOT(gammaSpinBoxSetValue(int)));
    connect(gammaSpinBox, SIGNAL(valueChanged(double)), this, SLOT(gammaSliderSetValue(double)));

    negativeCheckBox = new QCheckBox("negative");
    connect(negativeCheckBox, SIGNAL(clicked(bool)), this, SLOT(setNegative(bool)));

    layout = new QVBoxLayout(this);
    layoutBgc = new QGridLayout(this);
    layoutBgc->addWidget(new QLabel("Brightness", this), 0, 0);
    layoutBgc->addWidget(brightnessSlider, 0, 1);
    layoutBgc->addWidget(brightnessSpinBox, 0, 2 );
    layoutBgc->addWidget(negativeCheckBox, 0, 3);


    layoutBgc->addWidget(new QLabel("Contrast", this), 1, 0);
    layoutBgc->addWidget(contrastSlider, 1, 1);
    layoutBgc->addWidget(contrastSpinBox, 1, 2 );

    layoutBgc->addWidget(new QLabel("Gamma", this), 2, 0 );
    layoutBgc->addWidget(gammaSlider, 2, 1);
    layoutBgc->addWidget(gammaSpinBox, 2, 2 );

    buttonLayout = new QHBoxLayout(this);
    buttonLayout->addWidget(cancelButton);
    buttonLayout->addWidget(resetButton);
    buttonLayout->addWidget(okButton);

    layout->addLayout(layoutBgc);
    layout->addLayout(buttonLayout);
}
void Bgc::setImage(QImage *img){
    resultImage = img;
    orginalImage = *resultImage;
}
void Bgc::setNegative(bool state){
    if(state==true){
        brightnessSlider->setValue(255);
        brightnessSlider->setEnabled(false);
        brightnessSpinBox->setEnabled(false);
        contrastSlider->setEnabled(false);
        contrastSpinBox->setEnabled(false);
        gammaSlider->setEnabled(false);
        gammaSpinBox->setEnabled(false);
        correctionSlot();
    }else{
        brightnessSlider->setValue(0);
        brightnessSlider->setEnabled(true);
        brightnessSpinBox->setEnabled(true);
        contrastSlider->setEnabled(true);
        contrastSpinBox->setEnabled(true);
        gammaSlider->setEnabled(true);
        gammaSpinBox->setEnabled(true);
        correctionSlot();
    }
}
void Bgc::cancel(){
    reset();
    close();
}
void Bgc::reset(){
    brightnessSlider->setValue(0);
    contrastSlider->setValue(0);
    gammaSlider->setValue(10);
    negativeCheckBox->setChecked(false);
    emit updateImageSignal();
}
void Bgc::correctionSlot(){
    *resultImage = orginalImage;
    int b = brightnessSlider->value();
    int c = contrastSlider->value();
    int g = gammaSlider->value();
    correctionBgcLUT(resultImage,b,c,g);
    emit updateImageSignal();
}
void Bgc::gammaSpinBoxSetValue(int value){
    gammaSpinBox->setValue(intToGamma(value));
}
void Bgc::gammaSliderSetValue(double value){
    int result;
    if(value < 1.0)
        result = value*10;
    else
        result = value+9;
    gammaSlider->setValue(result);
}
void Bgc::initializeLUT(){
    for (int i=0; i<256; i++){
        brightnessLUT[i] = 0;
        contrastLUT[i] = 0;
        gammaLUT[i] = 0;
        negativeLUT[i] = 0;
    }
}
void Bgc::setBrightnessLUT(int value){
    for(int i=0; i<256; i++){
        brightnessLUT[i] = brightness(i,value);
        negativeLUT[i] = brightness(-i,value);
    }
}
void Bgc::setContrastLUT(int value){
    for(int i=0; i<256; i++){
        contrastLUT[i] = contrast(i,value);
    }
}
void Bgc::setGammaLUT(int value){
    for(int i=0; i<256; i++){
        gammaLUT[i] = gamma(i,value);
    }
}
void Bgc::correctionBgcLUT(QImage* image, int brightness, int contrast, int gamma){
    initializeLUT();
    setBrightnessLUT(brightness);
    setContrastLUT(contrast);
    setGammaLUT(gamma);
    uchar *p;
    int change = isGray(image);
    for(int y=0; y<image->height(); y++){
        p = image->scanLine(y);
        for(int x=0; x<change*image->width(); x++){
            if(brightness==255 && negativeCheckBox->isChecked())
                p[x] = negativeLUT[p[x]];
            else{
                p[x] = brightnessLUT[p[x]];
                p[x] = contrastLUT[p[x]];
                p[x] = gammaLUT[p[x]];
            }
        }
    }
}
