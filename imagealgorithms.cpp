#include <imagealgorithms.h>

int isGray(QImage* image){
    if (image->isGrayscale())
        return 1;
    return 4;
}
int clamp(int value, int min, int max){
    if(value>max) return max;
    if(value<min) return min;
    return value;
}
double clamp(double value, double min, double max){
    if(value>max) return max;
    if(value<min) return min;
    return value;
}
int brightness(int px, int value){
    return clamp(px + value, 0, 255);
}
int contrast(int px, int value){
    if(value >= 0 && value < 127)
        return clamp(qRound((127.0/(127-value))*(px-value)),0,255);
    else if(value==127){
            if (px <127)
                return 0;
            else
                return 255;
    }else
        return clamp(qRound(((127+value)/127.0)*px-value), 0, 255);
}
int gamma(int px, int value){
    return clamp(qRound(pow((px/255.0), intToGamma(value))*255.0),0,255);
}
double intToGamma(int gamma){
    if(gamma < 10)
        return gamma/10.0;
    return gamma - 9.0;
}
void rgbToCmyk(int red, int green, int blue, int &c, int &m, int &y, int &k){
        double cyjan = 1.0 - red/255.0;
        double magenta = 1.0 - green/255.0;
        double yellow = 1.0 - blue/255.0;
        double black = qMin(qMin(cyjan, magenta), yellow);

        cyjan = (cyjan-black)/(1.0-black);
        magenta =(magenta-black)/(1.0-black);
        yellow = (yellow-black)/(1.0-black);

        c = qRound(cyjan*255.0);
        m = qRound(magenta*255.0);
        y = qRound(yellow*255.0);
        k = qRound(black*255.0);
}
void cmykToRgb(int c, int m, int y, int k, int &r, int &g, int &b){
        double cyjan = c/255.0;
        double magenta = m/255.0;
        double yellow = y/255.0;
        double black = k/255.0;

        cyjan = cyjan*(1.0-black)+black;
        magenta = magenta*(1.0-black)+black;
        yellow = yellow*(1.0-black)+black;
        double red = 1.0 - cyjan;
        double green = 1.0 - magenta;
        double blue = 1.0 - yellow;

        r = qRound(red*255.0);
        g = qRound(green*255.0);
        b = qRound(blue*255.0);
}
void rgbToHsl(int r, int g, int b, double &h, double &s, double &l){
    double red = r/255.0;
    double green = g/255.0;
    double blue = b/255.0;

    double hue=0.0;
    double saturation=0.0;
    double lightness=0.0;

    double max = qMax(qMax(red, green),blue);
    double min = qMin(qMin(red, green),blue);
    double deltaM = max-min;

    lightness = (max+min)/2.0;

    // saturation
    if(lightness==0 || max==min){
        saturation = 0;
        hue = 0;
    }else if(lightness > 0 && lightness <= 0.5){
        saturation = deltaM/(2.0*lightness);
    }else if(lightness > 0.5){
        saturation = deltaM/(2.0-2.0*lightness);
    }

    // hue
    if(max==red && green >= blue){
        hue = 60.0*((green-blue)/deltaM);
    }else if(max==red && green < blue){
        hue = 60.0*((green-blue)/deltaM)+360.0;
    }else if(max==green){
        hue = 60.0*((blue-red)/deltaM)+120.0;
    }else if(max==blue){
        hue = 60.0*((red-green)/deltaM)+240.0;
    }

    h = hue;
    s = saturation;
    l = lightness;
}
void hslToRgb(double hue, double saturation, double lightness, int &red, int &green, int &blue){
    double rgb[3];
    double q,p, t[3];

    if(saturation==0){
        rgb[0] = lightness;
        rgb[1] = lightness;
        rgb[2] = lightness;
    }else if(saturation > 0){
        if(lightness<0.5){
            q = lightness*(1.0+saturation);
        }else{
            q = lightness + saturation -(lightness*saturation);
        }
        p = 2.0*lightness-q;
        hue = hue/360.0;
        t[0] = hue+0.33333;
        t[1] = hue;
        t[2] = hue-0.33333;
        for(int i=0; i<3; i++){
            if(t[i]<0)
                t[i]=t[i]+1.0;
            else if(t[i]>1.0)
                t[i] = t[i]-1.0;
        }
        for(int i=0; i<3; i++){
            if(t[i]<0.16666)
                rgb[i] = p+(q-p)*6.0*t[i];
            else if(t[i]>=0.16666 && t[i]<0.5)
                rgb[i] = q;
            else if(t[i]>=0.5 && t[i]<0.66666)
                rgb[i] = p+(q-p)*6.0*(0.66666-t[i]);
            else
                rgb[i] = p;
        }
    }
    red = qRound(rgb[0]*255.0);
    green = qRound(rgb[1]*255.0);
    blue = qRound(rgb[2]*255.0);
}
void rgbToXyz(int r, int g, int b, int &x, int &y, int &z){
    double sRgbMatrix[3][3];
    sRgbMatrix[0][0] = 0.41242;
    sRgbMatrix[0][1] = 0.21266;
    sRgbMatrix[0][2] = 0.01933;
    sRgbMatrix[1][0] = 0.35759;
    sRgbMatrix[1][1] = 0.71517;
    sRgbMatrix[1][2] = 0.11919;
    sRgbMatrix[2][0] = 0.18046;
    sRgbMatrix[2][1] = 0.07218;
    sRgbMatrix[2][2] = 0.95044;

    double red = pow(r/255.0, 2.2);
    double green = pow(g/255.0, 2.2);
    double blue = pow(b/255.0, 2.2);
    x = qRound((red*sRgbMatrix[0][0]+green*sRgbMatrix[1][0]+blue*sRgbMatrix[2][0])*255.0);
    y = qRound((red*sRgbMatrix[0][1]+green*sRgbMatrix[1][1]+blue*sRgbMatrix[2][1])*255.0);
    z = qRound((red*sRgbMatrix[0][2]+green*sRgbMatrix[1][2]+blue*sRgbMatrix[2][2])*255.0);
}
void xyzToRgb(int x, int y, int z, int &red, int &green, int &blue){
    double sRgbReverseMatrix[3][3];
    sRgbReverseMatrix[0][0] =  3.24071;
    sRgbReverseMatrix[0][1] = -0.96925;
    sRgbReverseMatrix[0][2] =  0.05563;
    sRgbReverseMatrix[1][0] = -1.53726;
    sRgbReverseMatrix[1][1] =  1.87599;
    sRgbReverseMatrix[1][2] = -0.20399,
    sRgbReverseMatrix[2][0] = -0.49857;
    sRgbReverseMatrix[2][1] =  0.04155;
    sRgbReverseMatrix[2][2] =  1.05707;

    double cieX = (x/255.0);
    double cieY = (y/255.0);
    double cieZ = (z/255.0);
    red = qRound(pow((cieX*sRgbReverseMatrix[0][0]+cieY*sRgbReverseMatrix[1][0]+cieZ*sRgbReverseMatrix[2][0]),1.0/2.2)*255.0);
    green = qRound(pow((cieX*sRgbReverseMatrix[0][1]+cieY*sRgbReverseMatrix[1][1]+cieZ*sRgbReverseMatrix[2][1]),1.0/2.2)*255.0);
    blue = qRound(pow((cieX*sRgbReverseMatrix[0][2]+cieY*sRgbReverseMatrix[1][2]+cieZ*sRgbReverseMatrix[2][2]),1.0/2.2)*255.0);
}
void xyzToLab(int x, int y, int z, int &l, int &a, int &b){
    double xRefD65 = 0.9505;
    double yRefD65 = 1.0000;
    double zRefD65 = 1.0891;
    double e = 0.008856;
    double k = 903.3;
    double xr = (x/255.0)/xRefD65;
    double yr = (y/255.0)/yRefD65;
    double zr = (z/255.0)/zRefD65;
    double labX;
    double labY;
    double labZ;

    if(xr >e){
        labX = pow(xr, 1.0/3.0);
    }else{
        labX = (k*xr+16.0)/116.0;
    }

    if(yr >e){
        labY = pow(yr, 1.0/3.0);
    }else{
        labY = (k*yr+16.0)/116.0;
    }

    if(zr >e){
        labZ = pow(zr, 1.0/3.0);
    }else{
        labZ = (k*zr+16.0)/116.0;
    }

    l = qRound((116.0*labY-16.0));
    a = qRound((500.0*(labX-labY)));
    b = qRound((200.0*(labY-labZ)));

}
void labToXyz(int li, int ai, int bi, int &x, int &y, int &z){
    double xRefD65 = 0.9505;
    double yRefD65 = 1.0000;
    double zRefD65 = 1.0891;
    double l = li;
    double a = ai;
    double b = bi;
    double e = 0.008856;
    double k = 903.3;
    double labX=0;
    double labY=0;
    double labZ=0;
    double xr=0;
    double yr=0;
    double zr=0;

    if(l > e*k){
        yr = ((l+16.0)/116.0)*((l+16.0)/116.0)*((l+16.0)/116.0);
    }else{
        yr = l/k;
    }

    if(yr > e){
        labY = (l+16.0)/116.0;
    }else{
        labY = (k*yr+16.0)/116.0;
    }
    labZ = labY - (b/200.0);
    labX = (a/500.0) + labY;

    if((labX*labX*labX)>e){
        xr = labX*labX*labX;
    }else{
        xr = (116.0*labX-16.0)/k;
    }

    if((labZ*labZ*labZ)>e){
        zr = labZ*labZ*labZ;
    }else{
        zr = (116.0*labZ-16.0)/k;
    }

    x = qRound(xr*xRefD65*255.0);
    y = qRound(yr*yRefD65*255.0);
    z = qRound(zr*zRefD65*255.0);
}
void xyzToLuv(int xi, int yi, int zi, int &l, int &u, int &v){
    double yRef = 1.0;
    double uRef = 0.2009;
    double vRef = 0.4610;
    double e = 0.008856;
    double k = 903.3;
    double luminance;

    double x = xi/255.0;
    double y = yi/255.0;
    double z = zi/255.0;
    double denominator = x+15.0*y+3.0*z;
    double pu = (4.0*x)/denominator;
    double pv = (9.0*y)/denominator;

    if((y/yRef) >e){
        luminance = 116.0*pow(y/yRef,1.0/3.0)-16.0;
    }else{
        luminance = k*(y/yRef);
    }

    l = qRound(luminance);
    u = qRound(13.0*luminance*(pu-uRef));
    v = qRound(13.0*luminance*(pv-vRef));
}
void luvToXyz(int li, int ui, int vi, int &x, int &y, int &z){
    double yRef = 1.0;
    double uRef = 0.2009;
    double vRef = 0.4610;
    double l = li;
    double u = ui;
    double v = vi;
    double e = 0.008856;
    double k = 903.3;
    double dy;

    double pu = (u/(13.0*l))+uRef;
    double pv = (v/(13.0*l))+vRef;

    if (l <= k*e){
       dy = yRef*(l/k);
    }else{
        dy = yRef*((l+16.0)/116.0)*((l+16.0)/116.0)*((l+16.0)/116.0);
    }

    x = qRound(dy*((9.0*pu)/(4.0*pv))*255.0);
    y = qRound(dy*255.0);
    z = qRound(dy*((12.0-3.0*pu-20.0*pv)/(4.0*pv))*255.0);
}
void rgbToLab(int red, int green, int blue, int &l, int &a, int &b){
    int x,y,z;
    rgbToXyz(red, green, blue, x, y, z);
    xyzToLab(x, y, z, l, a, b);
}
void labToRgb(int l, int a, int b, int &red, int &green, int &blue){
    int x,y,z;
    labToXyz(l, a, b, x, y, z);
    xyzToRgb(x, y, z, red, green, blue);
}
void rgbToLuv(int red, int green, int blue, int &l, int &u, int &v){
    int x,y,z;
    rgbToXyz(red, green, blue, x, y, z);
    xyzToLuv(x, y, z, l, u, v);
}
void luvToRgb(int l, int u, int v, int &red, int &green, int &blue){
    int x,y,z;
    luvToXyz(l, u, v, x, y, z);
    xyzToRgb(x, y, z, red, green, blue);
}

void replaceParts(QImage *img){
    int width = img->width();
    int height = img->height();
    QRgb *line,*line2,temp;

    for(int y=0; y<height/2; y++){
        line = (QRgb *)img->scanLine(y);
        line2 = (QRgb *)img->scanLine(height/2+y);
        for(int x=0; x<width; x++){
            if(x<width/2){
                temp = line[x];
                line[x] = line2[width/2+x];
                line2[width/2+x] = temp;
            }else{
                temp = line[x];
                line[x] = line2[x-width/2];
                line2[x-width/2] = temp;
            }
        }
    }
}
