#ifndef WATERMARKING_H
#define WATERMARKING_H

#include <QWidget>
#include <QImage>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QSpinBox>
#include <QLabel>
#include <algorithm>
#include <fftw-3.3.4/api/fftw3.h>

class Watermarking : public QWidget
{
    Q_OBJECT
    QHBoxLayout *sizeMarkLayout;
    QHBoxLayout *buttonLayout;
    QVBoxLayout *layout;
    QPushButton *okButton;
    QPushButton *cancelButton;
    QSlider *sizeMarkSlider;
    QSpinBox *sizeMarkSpinBox;

    QImage *resultImage;
    QImage orginalImage;
    void quickSort(double tab[], int tabIndex[], int, int);

public:
    Watermarking(QWidget *parent = 0);
    void setImage(QImage*);
    void addWatermarkInterface(int);
    int getSizeImage();

signals:

public slots:
    void addWatermark();
    void checkWatermark();

};

#endif // WATERMARKING_H
