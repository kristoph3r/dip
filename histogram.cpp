#include "histogram.h"

Histogram::Histogram(QWidget *parent) : QWidget(parent)
{
    infoHistWidget = new QWidget(this);
    infoHistWidget->setGeometry(rect().right()-300,rect().top(),255,350);

    colorChangeBox = new QComboBox();
    colorChangeBox->addItem("Value", 0);
    colorChangeBox->addItem("Red", 1);
    colorChangeBox->addItem("Green", 2);
    colorChangeBox->addItem("Blue", 3);
    connect(colorChangeBox, SIGNAL(activated(int)), this, SLOT(drawHist(int)));

    inputLevelsRadio = new QRadioButton("Input Levels");
    inputLevelsRadio->setChecked(true);
    outputLevelsRadio = new QRadioButton("Output Levels");
    leftSpinBox = new QSpinBox();
    leftSpinBox->setRange(0,254);
    connect(leftSpinBox, SIGNAL(valueChanged(int)), this, SLOT(strechingSlot()));
    rightSpinBox = new QSpinBox();
    rightSpinBox->setRange(1,255);
    rightSpinBox->setValue(255);
    connect(rightSpinBox, SIGNAL(valueChanged(int)), this, SLOT(strechingSlot()));

    okButton = new QPushButton("Ok", this);
    connect(okButton, SIGNAL(clicked()), this, SLOT(close()));
    cancelButton = new QPushButton("Cancel", this);
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));

    equalizationButton = new QPushButton("Equalization", this);
    connect(equalizationButton, SIGNAL(clicked()), this, SLOT(equalization()));

    resetButton = new QPushButton("Reset", this);
    connect(resetButton, SIGNAL(clicked()), this, SLOT(reset()));

    averageIntensity = new QLabel();
    varianceIntensity = new QLabel();
    standardDeviation = new QLabel();
    pixels = new QLabel();

    radioLayout = new QVBoxLayout();
    radioLayout->addWidget(inputLevelsRadio);
    radioLayout->addWidget(outputLevelsRadio);
    spinBoxLayout = new QHBoxLayout();
    spinBoxLayout->addWidget(leftSpinBox);
    spinBoxLayout->addWidget(rightSpinBox);
    buttonLayout = new QGridLayout();
    buttonLayout->setSpacing(3);
    buttonLayout->addWidget(resetButton,0,0);
    buttonLayout->addWidget(equalizationButton,0,1);
    buttonLayout->addWidget(cancelButton,1,0);
    buttonLayout->addWidget(okButton,1,1);
    infoHistLayout = new QVBoxLayout();
    infoHistLayout->addWidget(colorChangeBox);
    infoHistLayout->addLayout(radioLayout);
    infoHistLayout->addLayout(spinBoxLayout);
    infoHistLayout->addLayout(buttonLayout);
    infoHistLayout->addWidget(averageIntensity);
    infoHistLayout->addWidget(varianceIntensity);
    infoHistLayout->addWidget(standardDeviation);
    infoHistLayout->addWidget(pixels);

    infoHistWidget->setLayout(infoHistLayout);

    initializeHistArray();
}
void Histogram::initializeHistArray(){
    for(int i=0; i<256; i++){
        redHistArray[i]=0;
        greenHistArray[i]=0;
        blueHistArray[i]=0;
        valueHistArray[i]=0;
        cumulativeRedHistArray[i]=0;
        cumulativeGreenHistArray[i]=0;
        cumulativeBlueHistArray[i]=0;
    }
}
void Histogram::calculateValueHistogram(){
    float r,g,b;
    for(int i=0; i<256; i++){
        r = 0.32*redHistArray[i];
        g = 0.57*greenHistArray[i];
        b = 0.11*blueHistArray[i];
        valueHistArray[i]= r+g+b;
    }
}
int Histogram::isGray(QImage* image){
    if (image->isGrayscale())
        return 1;
    return 4;
}
void Histogram::setImage(QImage *img){
    histImage = img;
    orginalImage = *histImage;
    pixelsInImage = histImage->width()*histImage->height();
}
void Histogram::reset(){
    leftSpinBox->setValue(0);
    rightSpinBox->setValue(255);
    *histImage = orginalImage;
    emit updateImageSignal();
}
void Histogram::cancel(){
    reset();
    close();
}
int Histogram::maxValue(int tab[]){
    int maximum=1;
    for(int i=0; i<256; i++){
        if(tab[i]>maximum)
            maximum =tab[i];

    }
    return maximum;
}
int Histogram::clamp(int value, int min, int max){
    if(value>max) return max;
    if(value<min) return min;
    return value;
}
void Histogram::calculateHistogram(QImage* image){
    uchar *p;
    initializeHistArray();
    int change = isGray(image);
    for(int y=0; y<image->height(); y++){
        p = image->scanLine(y);
        for(int x=0; x<change*image->width(); x+=change){
            if(change == 1){
                blueHistArray[p[x]]++;
                greenHistArray[p[x]]++;
                redHistArray[p[x]]++;
            }else{
                blueHistArray[p[x]]++;
                greenHistArray[p[x+1]]++;
                redHistArray[p[x+2]]++;
            }
        }
    }
    calculateValueHistogram();
}
void Histogram::createCumulativeHistogram(){
    for(int i=0; i<256; i++){
        if(i==0){
            cumulativeRedHistArray[0] = redHistArray[0];
            cumulativeGreenHistArray[0] = greenHistArray[0];
            cumulativeBlueHistArray[0] = blueHistArray[0];
        }
        cumulativeRedHistArray[i] = redHistArray[i]+cumulativeRedHistArray[i-1];
        cumulativeGreenHistArray[i] = greenHistArray[i]+cumulativeGreenHistArray[i-1];
        cumulativeBlueHistArray[i] = blueHistArray[i]+cumulativeBlueHistArray[i-1];
        redLUT[i] = 255.0/pixelsInImage*cumulativeRedHistArray[i];
        greenLUT[i] = 255.0/pixelsInImage*cumulativeGreenHistArray[i];
        blueLUT[i] = 255.0/pixelsInImage*cumulativeBlueHistArray[i];
    }
}
void Histogram::equalization(){
    createCumulativeHistogram();
    uchar *p;
    for(int y=0; y<histImage->height(); y++){
        p = histImage->scanLine(y);
        for(int x=0; x<4*histImage->width(); x+=4){
                p[x+2] = redLUT[p[x+2]];
                p[x+1] = greenLUT[p[x+1]];
                p[x] = blueLUT[p[x]];
        }
    }
    emit updateImageSignal();
}
int Histogram::strechingLevels(int valuePxImage, int valueLeftSpinBox, int valueRightSpinBox){
    bool isInput = inputLevelsRadio->isChecked();
    if(isInput)
        return clamp((valuePxImage-valueLeftSpinBox)*(255.0/(valueRightSpinBox-valueLeftSpinBox)),0,255);
    return clamp((valuePxImage)*((valueRightSpinBox-valueLeftSpinBox)/255.0)+valueLeftSpinBox,0,255);
}
void Histogram::strechingSlot(){
    *histImage = orginalImage;
    int leftValue = leftSpinBox->value();
    int rightValue = rightSpinBox->value();
    int change = isGray(histImage);
    uchar *p;
    for(int y=0; y<histImage->height(); y++){
        p = histImage->scanLine(y);
        for(int x=0; x<change*histImage->width(); x+=change){
            p[x]= strechingLevels(p[x],leftValue, rightValue);
            p[x+1]= strechingLevels(p[x+1],leftValue, rightValue);
            p[x+2]= strechingLevels(p[x+2],leftValue, rightValue);
        }
    }
    emit updateImageSignal();
}

void Histogram::drawHist(int color){
    //color{0:value, 1:red, 2:green, 3:blue}
    colorHist = color;
}
float Histogram::calculateAvgIntensity(int color){
    //color{0:value, 1:red, 2:green, 3:blue}
    float avg=0.0;
    for(int i=0; i<256; i++){
        if(color == 1)
            avg += i*(redHistArray[i]/pixelsInImage);
        else if(color == 2)
            avg += i*(greenHistArray[i]/pixelsInImage);
        else if(color == 3)
            avg += i*(blueHistArray[i]/pixelsInImage);
        else
            avg += i*(valueHistArray[i]/pixelsInImage);
    }
    return avg;
}
float Histogram::calculateVarianceIntensity(int color){
    float variance=0.0;
    float avg = calculateAvgIntensity(color);
    for(int i=0; i<256; i++){
        if(color == 1)
            variance += (i-avg)*(i-avg)*redHistArray[i]/pixelsInImage;
        else if(color == 2)
            variance += (i-avg)*(i-avg)*greenHistArray[i]/pixelsInImage;
        else if(color == 3)
            variance += (i-avg)*(i-avg)*blueHistArray[i]/pixelsInImage;
        else
            variance += (i-avg)*(i-avg)*valueHistArray[i]/pixelsInImage;
    }
    return variance;
}
void Histogram::paintEvent(QPaintEvent *){

    QPainter painter(this);
    QPen colorPen;
    QColor rgbColor;
    int maximum=1;
    double scale;
    int steps = 25;
    int sizePlotArea = 255;
    calculateHistogram(histImage);

    // draw grid
    colorPen.setColor("#AAAAAA");
    colorPen.setStyle(Qt::DashLine);
    painter.setPen(colorPen);
    painter.translate(70,50);
    painter.drawRect(0,0,255,255);

        // vertical line
    for(int i=0; i<256/steps; i++)
        painter.drawLine(i*steps, 0, i*steps, sizePlotArea);

        // horizontal line
    for(int i=0; i<256/steps; i++)
        painter.drawLine(0, i*steps, sizePlotArea, i*steps);

    // draw value Histogram
    if (colorHist == 0){
        maximum = maxValue(valueHistArray);
        scale = 255.0/maximum;
        rgbColor= QColor(0,0,0,200);
        colorPen.setStyle(Qt::SolidLine);
        colorPen.setColor(QColor(rgbColor));
        painter.setPen(colorPen);
        for(int i=0; i<256; i++)
            painter.drawLine(i, sizePlotArea, i, sizePlotArea-valueHistArray[i]*scale);

        //value gradient
        QLinearGradient gradient(0,0,sizePlotArea+1,0);
        gradient.setColorAt(0, Qt::black);
        gradient.setColorAt(1, Qt::white);
        painter.fillRect(0,sizePlotArea+2, sizePlotArea+1,10, gradient);


    // draw red Histogram
    }else if(colorHist == 1){
        maximum = maxValue(redHistArray);
        scale = 255.0/maximum;
        rgbColor = QColor(255,0,0,200);
        colorPen.setStyle(Qt::SolidLine);
        colorPen.setColor(QColor(rgbColor));
        painter.setPen(colorPen);
        for(int i=0; i<256; i++)
            painter.drawLine(i, sizePlotArea, i, sizePlotArea-redHistArray[i]*scale);

        //red gradient
        QLinearGradient gradient(0,0,sizePlotArea+1,0);
        gradient.setColorAt(0, Qt::black);
        gradient.setColorAt(1, Qt::red);
        painter.fillRect(0,sizePlotArea+2, sizePlotArea+1,10, gradient);

    // draw green Histogram
    }else if(colorHist == 2){
        maximum = maxValue(greenHistArray);
        scale = 255.0/maximum;
        rgbColor = QColor(0,200,0,200);
        colorPen.setStyle(Qt::SolidLine);
        colorPen.setColor(QColor(rgbColor));
        painter.setPen(colorPen);
        for(int i=0; i<256; i++)
            painter.drawLine(i, sizePlotArea, i, sizePlotArea-greenHistArray[i]*scale);

        // green gradient
        QLinearGradient gradient(0,0,sizePlotArea+1,0);
        gradient.setColorAt(0, Qt::black);
        gradient.setColorAt(1, Qt::green);
        painter.fillRect(0,sizePlotArea+2, sizePlotArea+1,10, gradient);

    // draw blue Histogram
    }else if(colorHist == 3){
        maximum = maxValue(blueHistArray);
        scale = 255.0/maximum;
        rgbColor = QColor(0,0,255,200);
        colorPen.setStyle(Qt::SolidLine);
        colorPen.setColor(QColor(rgbColor));
        painter.setPen(colorPen);
        for(int i=0; i<256; i++)
            painter.drawLine(i, sizePlotArea, i, sizePlotArea-blueHistArray[i]*scale);

        // blue gradient
        QLinearGradient gradient(0,0,sizePlotArea+1,0);
        gradient.setColorAt(0, Qt::black);
        gradient.setColorAt(1, Qt::blue);
        painter.fillRect(0,sizePlotArea+2, sizePlotArea+1,10, gradient);
    }else
        colorHist = 0;
    update();

    colorPen.setColor(QColor(0,0,0,200));
    painter.setPen(colorPen);

    // draw scala

        // vertical scale
    painter.drawText(-50,5,QString("%1").arg(maximum));
    for(int i=steps*2; i<=200; i+=steps*2)
        painter.drawText(-50,i+5,QString("%1").arg(maximum-(i/(steps*2))*(maximum/5)));
    painter.drawText(-50,255,QString("0"));

        // horizontal scale
    painter.drawText(0,-10,"0");
    for(int i=steps*2; i<=200; i+=steps*2)
        painter.drawText(i-10,-10,QString("%1").arg(i));
    painter.drawText(245,-10,"255");

    averageIntensity->setText(QString("Average Intensity: %1").arg(calculateAvgIntensity(colorHist)));
    varianceIntensity->setText(QString("Variance of the Intensity: %1").arg(calculateVarianceIntensity(colorHist)));
    standardDeviation->setText(QString("Standard Deviation: %1").arg(sqrt(calculateVarianceIntensity(colorHist))));
    pixels->setText(QString("Pixels in Image: %1").arg(pixelsInImage));
}


