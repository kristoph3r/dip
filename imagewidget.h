#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QWidget>
#include <QImage>
#include <QPainter>

class ImageWidget : public QWidget
{
    Q_OBJECT
    QImage orginalImage;

protected:
    void paintEvent(QPaintEvent *);

signals:
    void repaintSignal();
public:
    ImageWidget(QWidget *parent = 0);
    void setImage(QImage);
    void removeImage();
    QImage *image() {return &orginalImage;}

};

#endif // IMAGEWIDGET_H
