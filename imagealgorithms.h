#ifndef IMAGEALGORITHMS_H
#define IMAGEALGORITHMS_H

#include <QImage>
#include <math.h>

int isGray(QImage*);
int clamp(int, int, int);
double clamp(double, double, double);

int brightness(int, int);
int contrast(int, int);
int gamma(int, int);
double intToGamma(int);
int rgbCorrection(int, int);

void rgbToCmyk(int, int, int, int &, int &, int &, int &);
void cmykToRgb(int, int, int, int, int &, int &, int &);
void rgbToHsl(int, int, int, double &, double &, double &);
void hslToRgb(double, double, double, int &, int &, int &);
void rgbToXyz(int, int, int, int &, int &, int &);
void xyzToRgb(int, int, int, int &, int &, int &);
void xyzToLab(int, int, int, int &, int &, int &);
void labToXyz(int, int, int, int &, int &, int &);
void xyzToLuv(int, int, int, int &, int &, int &);
void luvToXyz(int, int, int, int &, int &, int &);
void rgbToLab(int, int, int, int &, int &, int &);
void labToRgb(int, int, int, int &, int &, int &);
void rgbToLuv(int, int, int, int &, int &, int &);
void luvToRgb(int, int, int, int &, int &, int &);
void replaceParts(QImage *);

#endif // IMAGEALGORITHMS_H
