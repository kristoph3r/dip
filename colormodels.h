#ifndef COLORMODELS_H
#define COLORMODELS_H

#include <QWidget>
#include <QPushButton>
#include <QSlider>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QImage>
#include <QLayout>
#include <QGridLayout>
#include <QLabel>
#include <math.h>
#include <imagealgorithms.h>

class ColorModels : public QWidget
{
    Q_OBJECT

    QVBoxLayout *layout;
    QGridLayout *layoutColor;
    QHBoxLayout *layoutButton;

    QPushButton *okButton;
    QPushButton *cancelButton;
    QPushButton *resetButton;

    QImage *resultImage;
    QImage orginalImage;

    int getColorModel;

    // RGB Model
    QSlider *redSlider;
    QSlider *greenSlider;
    QSlider *blueSlider;
    QSpinBox *redSpinBox;
    QSpinBox *greenSpinBox;
    QSpinBox *blueSpinBox;
    int rgb[3];
    int redLUT[256];
    int greenLUT[256];
    int blueLUT[256];

    // CMYK Model
    QSlider *cyjanSlider;
    QSlider *magentaSlider;
    QSlider *yellowSlider;
    QSlider *blackSlider;
    QSpinBox *cyjanSpinBox;
    QSpinBox *magentaSpinBox;
    QSpinBox *yellowSpinBox;
    QSpinBox *blackSpinBox;
    int cmyk[4];

    // HSL Model
    QSlider *hueSlider;
    QSlider *saturationSlider;
    QSlider *lightnessSlider;
    QSpinBox *hueSpinBox;
    QSpinBox *saturationSpinBox;
    QSpinBox *lightnessSpinBox;
    double hsl[3];

    // CIE L*a*b* Model
    QSlider *luminanceLabSlider;
    QSlider *aSlider;
    QSlider *bSlider;
    QSpinBox *luminanceLabSpinBox;
    QSpinBox *aSpinBox;
    QSpinBox *bSpinBox;
    int lab[3];

    // CIE L*u*v* Model
    QSlider *luminanceLuvSlider;
    QSlider *uSlider;
    QSlider *vSlider;
    QSpinBox *luminanceLuvSpinBox;
    QSpinBox *uSpinBox;
    QSpinBox *vSpinBox;
    int luv[3];

    //RGB method
    void rgbInitializeLUT();
    void rgbCorectionLUT(QImage*, int, int, int);
    void setRgbLUT(int, int, int);

    // CMYK method
    void cmykInitialize();
    void cmykCorection(QImage*, int, int, int, int);

    // HSL method
    void hslInitialize();
    void hslCorection(QImage*, int, int, int);

    // CIE Method
    void labInitialize();
    void luvInitialize();
    void labCorection(QImage*, int, int, int);
    void luvCorection(QImage*, int, int, int);

public:
    ColorModels(QWidget *parent = 0);
    void setRgbModel();
    void setCmykModel();
    void setHslModel();
    void setLabModel();
    void setLuvModel();
    void setImage(QImage *);

signals:
    void updateImageSignal();

public slots:
    void cancel();
    void reset();
    void rgbCorectionSlot();
    void cmykCorectionSlot();
    void hslCorectionSlot();
    void labCorectionSlot();
    void luvCorectionSlot();
};

#endif // COLORMODELS_H
