#ifndef FILTERS_H
#define FILTERS_H

#include <QWidget>
#include <QPushButton>
#include <QSlider>
#include <QLabel>
#include <QSpinBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QImage>
#include <QLayout>
#include <QLineEdit>
#include <QTableWidget>
#include <QHeaderView>
#include <QScrollBar>
#include <QWidgetList>
#include <QTime>
#include <math.h>
#include <fftw-3.3.4/api/fftw3.h>
#include <imagealgorithms.h>

class Filters : public QWidget
{
    Q_OBJECT

    QSlider *radiusSlider;
    QSlider *maxRadiusSlider;
    QSlider *gainSlider;
    QSpinBox *gainSpinBox;
    QSpinBox *radiusSpinBox;
    QSpinBox *maxRadiusSpinBox;
    QPushButton *okButton;
    QPushButton *applyButton;
    QPushButton *cancelButton;
    QTableWidget *maskTable;
    QTableWidgetItem *maskTableItem;
    QHBoxLayout *convolutionLayout;
    QHBoxLayout *radiusLayout;
    QHBoxLayout *maxRadiusLayout;
    QHBoxLayout *gainLayout;
    QHBoxLayout *buttonLayout;
    QVBoxLayout *controlLayout;
    QVBoxLayout *gaussianLayout;
    QVBoxLayout *adaptiveLayout;
    QVBoxLayout *unsharpLayout;
    QCheckBox *labMode;
    QCheckBox *fftMode;
    QCheckBox *separateGaussMask;
    QTime *timer;
    QLabel *timerLabel;


    int getFilter;
    int colorScale;
    int sizeMask;
    int redMedianBucket[256];
    int greenMedianBucket[256];
    int blueMedianBucket[256];
    int greyMedianBucket[256];
    double weightMask;
    int ** mask;
    double **gaussianMask;
    int **redImageTab;
    int **greenImageTab;
    int **blueImageTab;
    int **greyImageTab;

    QImage *resultImage;
    QImage *image;
    QImage *maskImage;
    QImage orginalImage;

    void setElementMask(int);
    void setElementGaussianMask(int);
    void createMask(int);
    void createGaussianMask(int);
    void deleteMask(int);
    void deleteGaussianMask(int);
    void createImageTab(int, int);
    void imageToImageTab();
    void initializeMedianBucket();
    void convolution();
    void gaussian();
    void unsharp();
    void minimum();
    void maximum();
    void median();
    void adaptiveMedian();

public:
    Filters(QWidget *parent = 0);
    void setImage(QImage*);
    void setConvolutionFilter();
    void setGaussianFilter();
    void setUnsharpMaskFilter();
    void setMinimumFilter();
    void setMaximumFilter();
    void setMedianFilter();
    void setAdaptiveMedianFilter();

signals:
    void updateImageSignal();

public slots:
    void setSizeMaskTable(int);
    void resizeCell(int, int);
    void applySlot();
    void okSlot();
    void cancel();
};

#endif // FILTERS_H
