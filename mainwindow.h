#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "imagewidget.h"
#include "bgc.h"
#include "colormodels.h"
#include "histogram.h"
#include "filters.h"
#include "fouriertransform.h"
#include "compression.h"
#include "watermarking.h"
#include <QMainWindow>
#include <QFileDialog>
#include <QMenuBar>
#include <QAction>


class MainWindow : public QMainWindow
{
    Q_OBJECT

    QWidget *generalWidget;
    ImageWidget *imageWidget;
    Bgc *bgcWindow;
    ColorModels *colorModelWindow;
    Histogram *histogramWindow;
    Filters *filterWindow;
    FourierTransform *fourierWindow;
    Compression *compressObj;
    Watermarking *waterMark;
    QHBoxLayout *generalLayout;
    QMenu *fileMenu;
    QMenu *intensityMenu;
    QMenu *colorMenu;
    QMenu *filtersMenu;
    QMenu *fourierMenu;
    QMenu *aboutMenu;
    QMenu *compressionMenu;
    QAction *load;
    QAction *save;
    QString fileName;

    void createMenu();
    void setTitle();
    void setEnabledMenu();

public:
    MainWindow(QWidget *parent = 0);


public slots:
    void openFile();
    void closeFile();
    void bgcCorrection();
    void viewHistogram();
    void rgbColorModel();
    void cmykColorModel();
    void hslColorModel();
    void labColorModel();
    void luvColorModel();
    void convolutionTable();
    void gaussianFilter();
    void unsharpMaskFilter();
    void minimumFilter();
    void maximumFilter();
    void medianFilter();
    void adaptiveMedianFilter();
    void viewFourierTransform();
    void saveCompression();
    void loadCompression();
    void addWatermarkSlot();
    void checkWatermarkSlot();
};

#endif // MAINWINDOW_H

