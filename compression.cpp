#include "compression.h"
#include <QDebug>

Compression::Compression(QWidget *parent) :QWidget(parent)
{
    lvlSlider = new QSlider(Qt::Horizontal, this);
    lvlSlider->setRange(1,100);
    lvlSlider->setValue(50);
    lvlSpinBox = new QSpinBox();
    lvlSpinBox->setRange(1,100);
    lvlSpinBox->setValue(50);

    connect(lvlSlider, SIGNAL(valueChanged(int)), lvlSpinBox, SLOT(setValue(int)));
    connect(lvlSpinBox, SIGNAL(valueChanged(int)), lvlSlider, SLOT(setValue(int)));

    okButton = new QPushButton("OK", this);
    connect(okButton, SIGNAL(clicked()), this, SLOT(okSlot()));

    cancelButton = new QPushButton("Cancel", this);
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(close()));

    lvlLayout = new QHBoxLayout();
    lvlLayout->addWidget(new QLabel("Quality Level", this));
    lvlLayout->addWidget(lvlSlider);
    lvlLayout->addWidget(lvlSpinBox);

    buttonLayout = new QHBoxLayout();
    buttonLayout->addWidget(cancelButton);
    buttonLayout->addWidget(okButton);

    layout = new QVBoxLayout(this);
    layout->addLayout(lvlLayout);
    layout->addLayout(buttonLayout);

}
void Compression::setImage(QImage *img){
    resultImage = img;
    orginalImage = *resultImage;
}
void Compression::okSlot(){
    compress();
    close();
}
QString Compression::getPath(){
    return filePath;
}

void Compression::initLabTab(int width, int height){
    lTab = new int *[width];
    aTab = new int *[width];
    bTab = new int *[width];
    for(int i=0; i<width; i++){
        lTab[i] = new int[height];
        aTab[i] = new int[height];
        bTab[i] = new int[height];
    }
}
void Compression::setLabTab(int width, int height){
    initLabTab(width, height);
    QRgb *p;
    for(int y=0; y<height; y++){
        p = (QRgb*)resultImage->scanLine(y);
        for(int x=0; x<width; x++){
            rgbToLab(qRed(p[x]), qGreen(p[x]), qBlue(p[x]), lTab[x][y], aTab[x][y], bTab[x][y]);
        }
    }
}
void Compression::setRgbTab(int width, int height){
    QRgb *p;
    for(int y=0; y<height; y++){
        p = (QRgb*)resultImage->scanLine(y);
        for(int x=0; x<width; x++){
            labToRgb(lTab[x][y], aTab[x][y], bTab[x][y], rgb[0], rgb[1], rgb[2]);
            p[x] = qRgb(rgb[0],rgb[1],rgb[2]);
        }
    }
}

void Compression::save(QByteArray array){
    QFile file(QFileDialog::getSaveFileName());
    if(file.open(QFile::WriteOnly)){
         file.write(array);
    }
    file.close();
}
void Compression::load(){
    filePath = QFileDialog::getOpenFileName(this, "Open Image", "/Users/kristoph3r/Desktop", "Image Files (*.krx)");
    QFile file(filePath);
    if(file.open(QFile::ReadOnly)){
        while(!file.atEnd())
        {
            compressArray = file.readAll();
        }
    }
    file.close();
}
int Compression::levelCompression(int tabQ, int lvl){
    int result;
    if(lvl >50)
        result = (tabQ*(100-lvl))/50;
    else if(lvl <50)
        result = (tabQ*50)/lvl;
    else
        result = tabQ;
    if(result==0)
        result=1;
    return result;
}

void Compression::compress(){
    int width = resultImage->width();
    int height = resultImage->height();
    int lvlCompresion = lvlSlider->value();

    data  = QString("%1 %2 %3").arg(width).arg(height).arg(lvlCompresion);
    byteArray.append(data);

    setLabTab(width, height);

    int tabQL[64] = {16,11,10,16,24,40,51,61,
                     12,12,14,19,26,58,60,55,
                     14,13,16,24,40,57,69,56,
                     14,17,22,29,51,87,80,62,
                     18,22,37,56,68,109,103,77,
                     24,35,55,64,81,104,113,92,
                     49,64,78,87,103,121,120,101,
                     72,92,95,98,112,100,103,99};

    int tabQC[64] = {17,18,24,47,99,99,99,99,
                     18,21,26,66,99,99,99,99,
                     24,26,56,99,99,99,99,99,
                     47,66,99,99,99,99,99,99,
                     99,99,99,99,99,99,99,99,
                     99,99,99,99,99,99,99,99,
                     99,99,99,99,99,99,99,99,
                     99,99,99,99,99,99,99,99};

    int tabZZ[64] = {0,1,5,6,14,15,27,28,
                     2,4,7,13,16,26,29,42,
                     3,8,12,17,25,30,41,43,
                     9,11,18,24,31,40,44,53,
                     10,19,23,32,39,45,52,54,
                     20,22,33,38,46,51,55,60,
                     21,34,37,47,50,56,59,61,
                     35,36,48,49,57,58,62,63};

    for(int i=0; i<64; i++){
        tabQL[i] = levelCompression(tabQL[i], lvlCompresion);
        tabQC[i] = levelCompression(tabQC[i], lvlCompresion);
    }
    double *inL, *inA, *inB, *outL, *outA, *outB;
    int tabCL[64], tabCA[64], tabCB[64];
    inL = (double*)fftw_malloc(sizeof(double)*64);
    inA = (double*)fftw_malloc(sizeof(double)*64);
    inB = (double*)fftw_malloc(sizeof(double)*64);
    outL = (double*)fftw_malloc(sizeof(double)*64);
    outA = (double*)fftw_malloc(sizeof(double)*64);
    outB = (double*)fftw_malloc(sizeof(double)*64);


    fftw_plan plan;
    int jMax = height/8;
    int kMax = width/8;

    for(int j=0; j<jMax; j++){
        for(int k=0; k<kMax; k++){
            for(int i=0; i<64; i++){
                    int x = i%8+k*8;
                    int y = i/8+j*8;
                    inL[i] = lTab[x][y];
                    inA[i] = aTab[x][y]+128;
                    inB[i] = bTab[x][y]+128;
            }

            plan = fftw_plan_r2r_2d(8, 8, inL, outL, FFTW_REDFT10, FFTW_REDFT10, FFTW_ESTIMATE);
            fftw_execute(plan);
            fftw_destroy_plan(plan);

            plan = fftw_plan_r2r_2d(8, 8, inA, outA, FFTW_REDFT10, FFTW_REDFT10, FFTW_ESTIMATE);
            fftw_execute(plan);
            fftw_destroy_plan(plan);

            plan = fftw_plan_r2r_2d(8, 8, inB, outB, FFTW_REDFT10, FFTW_REDFT10, FFTW_ESTIMATE);
            fftw_execute(plan);
            fftw_destroy_plan(plan);

            for(int i=0; i<64; i++){
                int z = tabZZ[i];
                tabCL[z] = outL[i]/tabQL[i];
                tabCA[z] = outA[i]/tabQC[i];
                tabCB[z] = outB[i]/tabQC[i];
            }

            for(int i=0; i<64; i++){
                data = QString(" %1 %2 %3").arg(tabCL[i]).arg(tabCA[i]).arg(tabCB[i]);
                byteArray.append(data);
            }

        }
    }
    compressArray = qCompress(byteArray,-1);
    save(compressArray);
}
QImage Compression::decompress(){
    int tabQL[64] = {16,11,10,16,24,40,51,61,
                     12,12,14,19,26,58,60,55,
                     14,13,16,24,40,57,69,56,
                     14,17,22,29,51,87,80,62,
                     18,22,37,56,68,109,103,77,
                     24,35,55,64,81,104,113,92,
                     49,64,78,87,103,121,120,101,
                     72,92,95,98,112,100,103,99};

    int tabQC[64] = {17,18,24,47,99,99,99,99,
                     18,21,26,66,99,99,99,99,
                     24,26,56,99,99,99,99,99,
                     47,66,99,99,99,99,99,99,
                     99,99,99,99,99,99,99,99,
                     99,99,99,99,99,99,99,99,
                     99,99,99,99,99,99,99,99,
                     99,99,99,99,99,99,99,99};

    int tabZZ[64] = {0,1,5,6,14,15,27,28,
                     2,4,7,13,16,26,29,42,
                     3,8,12,17,25,30,41,43,
                     9,11,18,24,31,40,44,53,
                     10,19,23,32,39,45,52,54,
                     20,22,33,38,46,51,55,60,
                     21,34,37,47,50,56,59,61,
                     35,36,48,49,57,58,62,63};
    load();
    byteArray = qUncompress(compressArray);
    data = QString(byteArray);
    QStringList list = data.split(" ");

    int width = list[0].toInt();
    int height = list[1].toInt();
    int lvlCompresion = list[2].toInt();
    for(int i=0; i<64; i++){
        tabQL[i] = levelCompression(tabQL[i], lvlCompresion);
        tabQC[i] = levelCompression(tabQC[i], lvlCompresion);
    }
    initLabTab(width, height);
    img = new QImage(width, height, QImage::Format_RGB32);
    setImage(img);

    double *inL, *inA, *inB, *outL, *outA, *outB;
    int tabCL[64], tabCA[64], tabCB[64];
    inL = (double*)fftw_malloc(sizeof(double)*64);
    inA = (double*)fftw_malloc(sizeof(double)*64);
    inB = (double*)fftw_malloc(sizeof(double)*64);
    outL = (double*)fftw_malloc(sizeof(double)*64);
    outA = (double*)fftw_malloc(sizeof(double)*64);
    outB = (double*)fftw_malloc(sizeof(double)*64);
    fftw_plan plan;

    int jMax = height/8;
    int kMax = width/8;
    int nrBlock = 0;

    for(int j=0; j<jMax; j++){
        for(int k=0; k<kMax; k++){
            nrBlock = j*kMax+k;
            for(int i=0; i<64; i++){
                int z = tabZZ[i];
                tabCL[i] = list[(z*3+3)+nrBlock*192].toInt();
                tabCA[i] = list[(z*3+4)+nrBlock*192].toInt();
                tabCB[i] = list[(z*3+5)+nrBlock*192].toInt();
            }
            for(int i=0; i<64; i++){
                outL[i] = tabCL[i]*tabQL[i];
                outA[i] = tabCA[i]*tabQC[i];
                outB[i] = tabCB[i]*tabQC[i];
            }

            plan = fftw_plan_r2r_2d(8, 8, outL, inL, FFTW_REDFT01, FFTW_REDFT01, FFTW_ESTIMATE);
            fftw_execute(plan);
            fftw_destroy_plan(plan);

            plan = fftw_plan_r2r_2d(8, 8, outA, inA, FFTW_REDFT01, FFTW_REDFT01, FFTW_ESTIMATE);
            fftw_execute(plan);
            fftw_destroy_plan(plan);

            plan = fftw_plan_r2r_2d(8, 8, outB, inB, FFTW_REDFT01, FFTW_REDFT01, FFTW_ESTIMATE);
            fftw_execute(plan);
            fftw_destroy_plan(plan);

            for(int i=0; i<64; i++){
                int x = i%8+k*8;
                int y = i/8+j*8;

                lTab[x][y] = qRound(inL[i]/256.0);
                aTab[x][y] = qRound(inA[i]/256.0)-128;
                bTab[x][y] = qRound(inB[i]/256.0)-128;
            }
        }
    }
    setRgbTab(width, height);
    orginalImage = *resultImage;
    return orginalImage;
    close();
}
