#ifndef COMPRESSION_H
#define COMPRESSION_H

#include <QWidget>
#include <QFileDialog>
#include <QFile>
#include <QSlider>
#include <QSpinBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <imagealgorithms.h>
#include <fftw-3.3.4/api/fftw3.h>

class Compression : public QWidget
{
    Q_OBJECT
    QImage *resultImage;
    QImage orginalImage;
    QImage *img;
    QSlider *lvlSlider;
    QSpinBox *lvlSpinBox;
    QHBoxLayout *lvlLayout;
    QHBoxLayout *buttonLayout;
    QVBoxLayout *layout;
    QPushButton *okButton;
    QPushButton *cancelButton;
    QByteArray byteArray;
    QByteArray compressArray;
    QString data;
    QString filePath;
    int lab[3];
    int **lTab;
    int **aTab;
    int **bTab;
    int rgb[3];

    void initLabTab(int, int);
    void setLabTab(int, int);
    void setRgbTab(int, int);
    void save(QByteArray);
    void load();
    int levelCompression(int,int);

public:
    Compression(QWidget *parent = 0);
    void setImage(QImage*);
    QString getPath();

signals:

public slots:
    void okSlot();
    void compress();
    QImage decompress();

};

#endif // COMPRESSION_H
