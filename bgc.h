#ifndef BGC_H
#define BGC_H

#include <QWidget>
#include <QPushButton>
#include <QSlider>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QImage>
#include <QLayout>
#include <QGridLayout>
#include <QLabel>
#include <QCheckBox>
#include <imagealgorithms.h>

class Bgc : public QWidget  //Brightness, Gamma, Contrast
{
    Q_OBJECT

    QVBoxLayout *layout;
    QGridLayout *layoutBgc;
    QHBoxLayout *buttonLayout;

    QPushButton *okButton;
    QPushButton *cancelButton;
    QPushButton *resetButton;

    QSlider *brightnessSlider;
    QSlider *contrastSlider;
    QSlider *gammaSlider;

    QSpinBox *brightnessSpinBox;
    QSpinBox *contrastSpinBox;
    QDoubleSpinBox *gammaSpinBox;

    QCheckBox *negativeCheckBox;

    QImage *resultImage;
    QImage orginalImage;


    //LUT
    int brightnessLUT[256];
    int contrastLUT[256];
    int gammaLUT[256];
    int negativeLUT[256];

    void initializeLUT();
    void setBrightnessLUT(int);
    void setContrastLUT(int);
    void setGammaLUT(int);
    void correctionBgcLUT(QImage*, int, int, int);

public:
    Bgc(QWidget *parent = 0);
    void setImage(QImage*);

signals:
    void updateImageSignal();

public slots:
    void correctionSlot();
    void gammaSpinBoxSetValue(int);
    void gammaSliderSetValue(double);
    void setNegative(bool);
    void cancel();
    void reset();

};

#endif // BGC_H
