#include "watermarking.h"
#include <QDebug>
#include <math.h>

Watermarking::Watermarking(QWidget *parent) : QWidget(parent)
{

}
void Watermarking::setImage(QImage *img){
    resultImage = img;
    orginalImage = *resultImage;
}
int Watermarking::getSizeImage(){
    int width = resultImage->width();
    int height = resultImage->height();
    int size =width*height;
    return size;
}
void Watermarking::quickSort(double tab[],int tabIndex[], int left, int right){
    int i = left;
    int j = right;
    double x = qAbs(tab[(left+right)/2]);
    do{
        while(qAbs(tab[i]) < x)
            i++;
        while(qAbs(tab[j]) > x)
            j--;
        if(i<= j){
            qSwap(tab[i], tab[j]);
            qSwap(tabIndex[i], tabIndex[j]);
            i++;
            j--;
        }
    }while(i<= j);
    if(left <j )
        quickSort(tab, tabIndex, left, j);
    if(right > i)
        quickSort(tab, tabIndex, i, right);
}

void Watermarking::addWatermarkInterface(int size){
    sizeMarkSlider = new QSlider(Qt::Horizontal, this);
    sizeMarkSlider->setRange(1,size);
    sizeMarkSpinBox = new QSpinBox();
    sizeMarkSpinBox->setRange(1,size);

    connect(sizeMarkSlider, SIGNAL(valueChanged(int)), sizeMarkSpinBox, SLOT(setValue(int)));
    connect(sizeMarkSpinBox, SIGNAL(valueChanged(int)), sizeMarkSlider, SLOT(setValue(int)));

    okButton = new QPushButton("OK", this);
    connect(okButton, SIGNAL(clicked()), this, SLOT(addWatermark()));

    cancelButton = new QPushButton("Cancel", this);
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(close()));

    sizeMarkLayout = new QHBoxLayout();
    sizeMarkLayout->addWidget(new QLabel("Size Watermark", this));
    sizeMarkLayout->addWidget(sizeMarkSlider);
    sizeMarkLayout->addWidget(sizeMarkSpinBox);

    buttonLayout = new QHBoxLayout();
    buttonLayout->addWidget(cancelButton);
    buttonLayout->addWidget(okButton);

    layout = new QVBoxLayout(this);
    layout->addLayout(sizeMarkLayout);
    layout->addLayout(buttonLayout);

}

void Watermarking::addWatermark(){
    int width = resultImage->width();
    int height = resultImage->height();
    int size =width*height;

    double *inRed, *inGreen, *inBlue, *outRed, *outGreen, *outBlue;
    double sortRed[size], sortGreen[size], sortBlue[size];
    int indexRed[size], indexGreen[size], indexBlue[size];
    inRed = (double*)fftw_malloc(sizeof(double)*size);
    inGreen = (double*)fftw_malloc(sizeof(double)*size);
    inBlue = (double*)fftw_malloc(sizeof(double)*size);
    outRed = (double*)fftw_malloc(sizeof(double)*size);
    outGreen = (double*)fftw_malloc(sizeof(double)*size);
    outBlue = (double*)fftw_malloc(sizeof(double)*size);

    QRgb *p = (QRgb*)resultImage->bits();
    for(int i=0; i<size; i++){
        inRed[i] = qRed(p[i]);
        inGreen[i] = qGreen(p[i]);
        inBlue[i] = qBlue(p[i]);
    }

    fftw_plan plan;
    plan = fftw_plan_r2r_2d(width, height, inRed, outRed, FFTW_REDFT10, FFTW_REDFT10, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    plan = fftw_plan_r2r_2d(width, height, inGreen, outGreen, FFTW_REDFT10, FFTW_REDFT10, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    plan = fftw_plan_r2r_2d(width, height, inBlue, outBlue, FFTW_REDFT10, FFTW_REDFT10, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

   // int k = sizeMarkSlider->value();

    for(int i=0; i<size; i++){
        sortRed[i] = outRed[i];
        sortGreen[i] = outGreen[i];
        sortBlue[i] = outBlue[i];
        indexRed[i] = i;
       // indexGreen[0] = i;
       // indexBlue[i] = i;
    }

    quickSort(sortRed, indexRed, 0, size);
    //quickSort(sortGreen, indexGreen, 0, size-1);
    //quickSort(sortBlue, indexBlue, 0, size-1);

}
void Watermarking::checkWatermark(){

}
