#include "imagewidget.h"

ImageWidget::ImageWidget(QWidget *parent) : QWidget(parent)
{
    orginalImage = QImage();
}
void ImageWidget::setImage(QImage img){
    orginalImage = img;
    update();
}
void ImageWidget::removeImage(){
    orginalImage = QImage();
    update();
}
void ImageWidget::paintEvent(QPaintEvent *){
    QPainter paint(this);
    if(!orginalImage.isNull())
        paint.drawImage(0, 0, orginalImage);
        emit repaintSignal();

}
