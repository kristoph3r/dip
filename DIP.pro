#-------------------------------------------------
#
# Project created by QtCreator 2014-10-10T11:18:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DIP
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    imagewidget.cpp \
    bgc.cpp \
    histogram.cpp \
    colormodels.cpp \
    filters.cpp \
    imagealgorithms.cpp \
    fouriertransform.cpp \
    compression.cpp \
    watermarking.cpp

HEADERS  += mainwindow.h \
    imagewidget.h \
    bgc.h \
    histogram.h \
    colormodels.h \
    filters.h \
    imagealgorithms.h \
    fouriertransform.h \
    compression.h \
    watermarking.h

FORMS    +=
LIBS = -L/Users/kristoph3r/Documents/ProjektyQt/DIP/fftw-3.3.4 -lfftw3
INCLUDEPATH = -I/Users/kristoph3r/Documents/ProjektyQt/DIP/fftw-3.3.4
