#include "colormodels.h"

ColorModels::ColorModels(QWidget *parent) : QWidget(parent)
{
    layout = new QVBoxLayout(this);
    layoutColor = new QGridLayout(this);
    layoutButton = new QHBoxLayout(this);

    okButton = new QPushButton("OK", this);
    connect(okButton, SIGNAL(clicked()), this, SLOT(close()));

    cancelButton = new QPushButton("Cancel", this);
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));

    resetButton = new QPushButton("Reset", this);
    connect(resetButton, SIGNAL(clicked()), this, SLOT(reset()));

    layoutButton->addWidget(cancelButton);
    layoutButton->addWidget(resetButton);
    layoutButton->addWidget(okButton);

    layout->addLayout(layoutColor);
    layout->addLayout(layoutButton);

}
void ColorModels::setImage(QImage *img){
    resultImage = img;
    orginalImage = *resultImage;
}
void ColorModels::cancel(){
    reset();
    close();
}
void ColorModels::reset(){
    if(getColorModel==0){
        redSlider->setValue(0);
        greenSlider->setValue(0);
        blueSlider->setValue(0);
    }
    if(getColorModel==1){
        cyjanSlider->setValue(0);
        magentaSlider->setValue(0);
        yellowSlider->setValue(0);
        blackSlider->setValue(0);
    }
    if(getColorModel==2){
        hueSlider->setValue(0);
        saturationSlider->setValue(0);
        lightnessSlider->setValue(0);
    }
    if(getColorModel==3){
        luminanceLabSlider->setValue(0);
        aSlider->setValue(0);
        bSlider->setValue(0);
        *resultImage = orginalImage;
    }
    if(getColorModel==4){
        luminanceLuvSlider->setValue(0);
        uSlider->setValue(0);
        vSlider->setValue(0);
        *resultImage = orginalImage;
    }
    emit updateImageSignal();

}
// set Color Model Layout
void ColorModels::setRgbModel(){
    getColorModel = 0;
    redSlider = new QSlider(Qt::Horizontal, this);
    redSlider->setRange(-255,255);
    redSlider->setMinimumWidth(250);

    redSpinBox = new QSpinBox();
    redSpinBox->setRange(-255,255);
    connect(redSlider, SIGNAL(valueChanged(int)), redSpinBox, SLOT(setValue(int)));
    connect(redSpinBox, SIGNAL(valueChanged(int)), redSlider, SLOT(setValue(int)));
    connect(redSlider, SIGNAL(valueChanged(int)), this, SLOT(rgbCorectionSlot()));

    greenSlider = new QSlider(Qt::Horizontal, this);
    greenSlider->setRange(-255,255);
    greenSlider->setMinimumWidth(250);

    greenSpinBox = new QSpinBox();
    greenSpinBox->setRange(-255,255);
    connect(greenSlider, SIGNAL(valueChanged(int)), greenSpinBox, SLOT(setValue(int)));
    connect(greenSpinBox, SIGNAL(valueChanged(int)), greenSlider, SLOT(setValue(int)));
    connect(greenSlider, SIGNAL(valueChanged(int)), this, SLOT(rgbCorectionSlot()));

    blueSlider = new QSlider(Qt::Horizontal, this);
    blueSlider->setRange(-255,255);
    blueSlider->setMinimumWidth(250);

    blueSpinBox = new QSpinBox();
    blueSpinBox->setRange(-255,255);
    connect(blueSlider, SIGNAL(valueChanged(int)), blueSpinBox, SLOT(setValue(int)));
    connect(blueSpinBox, SIGNAL(valueChanged(int)), blueSlider, SLOT(setValue(int)));
    connect(blueSlider, SIGNAL(valueChanged(int)), this, SLOT(rgbCorectionSlot()));

    layoutColor ->addWidget(new QLabel("Red", this), 0, 0);
    layoutColor ->addWidget(redSlider, 0, 1);
    layoutColor ->addWidget(redSpinBox, 0, 2 );

    layoutColor ->addWidget(new QLabel("Green", this), 1, 0);
    layoutColor ->addWidget(greenSlider, 1, 1);
    layoutColor ->addWidget(greenSpinBox, 1, 2 );

    layoutColor ->addWidget(new QLabel("Blue", this), 2, 0);
    layoutColor ->addWidget(blueSlider, 2, 1);
    layoutColor ->addWidget(blueSpinBox, 2, 2 );
}
void ColorModels::setCmykModel(){
    getColorModel = 1;
    cyjanSlider = new QSlider(Qt::Horizontal, this);
    cyjanSlider->setRange(-255,255);
    cyjanSlider->setMinimumWidth(250);

    cyjanSpinBox = new QSpinBox();
    cyjanSpinBox->setRange(-255,255);
    connect(cyjanSlider, SIGNAL(valueChanged(int)), cyjanSpinBox, SLOT(setValue(int)));
    connect(cyjanSpinBox, SIGNAL(valueChanged(int)), cyjanSlider, SLOT(setValue(int)));
    connect(cyjanSlider, SIGNAL(valueChanged(int)), this, SLOT(cmykCorectionSlot()));

    magentaSlider = new QSlider(Qt::Horizontal, this);
    magentaSlider->setRange(-255,255);
    magentaSlider->setMinimumWidth(250);

    magentaSpinBox = new QSpinBox();
    magentaSpinBox->setRange(-255,255);
    connect(magentaSlider, SIGNAL(valueChanged(int)), magentaSpinBox, SLOT(setValue(int)));
    connect(magentaSpinBox, SIGNAL(valueChanged(int)), magentaSlider, SLOT(setValue(int)));
    connect(magentaSlider, SIGNAL(valueChanged(int)), this, SLOT(cmykCorectionSlot()));

    yellowSlider = new QSlider(Qt::Horizontal, this);
    yellowSlider->setRange(-255,255);
    yellowSlider->setMinimumWidth(250);

    yellowSpinBox = new QSpinBox();
    yellowSpinBox->setRange(-255,255);
    connect(yellowSlider, SIGNAL(valueChanged(int)), yellowSpinBox, SLOT(setValue(int)));
    connect(yellowSpinBox, SIGNAL(valueChanged(int)), yellowSlider, SLOT(setValue(int)));
    connect(yellowSlider, SIGNAL(valueChanged(int)), this, SLOT(cmykCorectionSlot()));

    blackSlider = new QSlider(Qt::Horizontal, this);
    blackSlider->setRange(-255,255);
    blackSlider->setMinimumWidth(250);

    blackSpinBox = new QSpinBox();
    blackSpinBox->setRange(-255,255);
    connect(blackSlider, SIGNAL(valueChanged(int)), blackSpinBox, SLOT(setValue(int)));
    connect(blackSpinBox, SIGNAL(valueChanged(int)), blackSlider, SLOT(setValue(int)));
    connect(blackSlider, SIGNAL(valueChanged(int)), this, SLOT(cmykCorectionSlot()));

    layoutColor ->addWidget(new QLabel("Cyjan", this), 0, 0);
    layoutColor ->addWidget(cyjanSlider, 0, 1);
    layoutColor ->addWidget(cyjanSpinBox, 0, 2 );

    layoutColor ->addWidget(new QLabel("Magenta", this), 1, 0);
    layoutColor ->addWidget(magentaSlider, 1, 1);
    layoutColor ->addWidget(magentaSpinBox, 1, 2 );

    layoutColor ->addWidget(new QLabel("Yellow", this), 2, 0);
    layoutColor ->addWidget(yellowSlider, 2, 1);
    layoutColor ->addWidget(yellowSpinBox, 2, 2 );

    layoutColor ->addWidget(new QLabel("Black", this), 3, 0);
    layoutColor ->addWidget(blackSlider, 3, 1);
    layoutColor ->addWidget(blackSpinBox, 3, 2 );
}
void ColorModels::setHslModel(){
    getColorModel = 2;
    hueSlider = new QSlider(Qt::Horizontal, this);
    hueSlider->setRange(-180,180);
    hueSlider->setMinimumWidth(250);

    hueSpinBox = new QSpinBox();
    hueSpinBox->setRange(-180,180);
    connect(hueSlider, SIGNAL(valueChanged(int)), hueSpinBox, SLOT(setValue(int)));
    connect(hueSpinBox, SIGNAL(valueChanged(int)), hueSlider, SLOT(setValue(int)));
    connect(hueSlider, SIGNAL(valueChanged(int)), this, SLOT(hslCorectionSlot()));

    saturationSlider = new QSlider(Qt::Horizontal, this);
    saturationSlider->setRange(-100,100);
    saturationSlider->setMinimumWidth(250);

    saturationSpinBox = new QSpinBox();
    saturationSpinBox->setRange(-100,100);
    connect(saturationSlider, SIGNAL(valueChanged(int)), saturationSpinBox, SLOT(setValue(int)));
    connect(saturationSpinBox, SIGNAL(valueChanged(int)), saturationSlider, SLOT(setValue(int)));
    connect(saturationSlider, SIGNAL(valueChanged(int)), this, SLOT(hslCorectionSlot()));

    lightnessSlider = new QSlider(Qt::Horizontal, this);
    lightnessSlider->setRange(-100,100);
    lightnessSlider->setMinimumWidth(250);

    lightnessSpinBox = new QSpinBox();
    lightnessSpinBox->setRange(-100,100);
    connect(lightnessSlider, SIGNAL(valueChanged(int)), lightnessSpinBox, SLOT(setValue(int)));
    connect(lightnessSpinBox, SIGNAL(valueChanged(int)), lightnessSlider, SLOT(setValue(int)));
    connect(lightnessSlider, SIGNAL(valueChanged(int)), this, SLOT(hslCorectionSlot()));

    layoutColor ->addWidget(new QLabel("Hue", this), 0, 0);
    layoutColor ->addWidget(hueSlider, 0, 1);
    layoutColor ->addWidget(hueSpinBox, 0, 2 );

    layoutColor ->addWidget(new QLabel("Saturation", this), 1, 0);
    layoutColor ->addWidget(saturationSlider, 1, 1);
    layoutColor ->addWidget(saturationSpinBox, 1, 2 );

    layoutColor ->addWidget(new QLabel("Lightness", this), 2, 0);
    layoutColor ->addWidget(lightnessSlider, 2, 1);
    layoutColor ->addWidget(lightnessSpinBox, 2, 2 );
}
void ColorModels::setLabModel(){
    getColorModel = 3;
    luminanceLabSlider = new QSlider(Qt::Horizontal, this);
    luminanceLabSlider->setRange(-150,150);
    luminanceLabSlider->setMinimumWidth(250);

    luminanceLabSpinBox = new QSpinBox();
    luminanceLabSpinBox->setRange(-150,150);
    connect(luminanceLabSlider, SIGNAL(valueChanged(int)), luminanceLabSpinBox, SLOT(setValue(int)));
    connect(luminanceLabSpinBox, SIGNAL(valueChanged(int)), luminanceLabSlider, SLOT(setValue(int)));
    connect(luminanceLabSlider, SIGNAL(valueChanged(int)), this, SLOT(labCorectionSlot()));

    aSlider = new QSlider(Qt::Horizontal, this);
    aSlider->setRange(-128,127);
    aSlider->setMinimumWidth(250);

    aSpinBox = new QSpinBox();
    aSpinBox->setRange(-128,127);
    connect(aSlider, SIGNAL(valueChanged(int)), aSpinBox, SLOT(setValue(int)));
    connect(aSpinBox, SIGNAL(valueChanged(int)), aSlider, SLOT(setValue(int)));
    connect(aSlider, SIGNAL(valueChanged(int)), this, SLOT(labCorectionSlot()));

    bSlider = new QSlider(Qt::Horizontal, this);
    bSlider->setRange(-128,127);
    bSlider->setMinimumWidth(250);

    bSpinBox = new QSpinBox();
    bSpinBox->setRange(-128,127);
    connect(bSlider, SIGNAL(valueChanged(int)), bSpinBox, SLOT(setValue(int)));
    connect(bSpinBox, SIGNAL(valueChanged(int)), bSlider, SLOT(setValue(int)));
    connect(bSlider, SIGNAL(valueChanged(int)), this, SLOT(labCorectionSlot()));

    layoutColor ->addWidget(new QLabel("L*", this), 0, 0);
    layoutColor ->addWidget(luminanceLabSlider, 0, 1);
    layoutColor ->addWidget(luminanceLabSpinBox, 0, 2 );

    layoutColor ->addWidget(new QLabel("a*", this), 1, 0);
    layoutColor ->addWidget(aSlider, 1, 1);
    layoutColor ->addWidget(aSpinBox, 1, 2 );

    layoutColor ->addWidget(new QLabel("b*", this), 2, 0);
    layoutColor ->addWidget(bSlider, 2, 1);
    layoutColor ->addWidget(bSpinBox, 2, 2 );
}
void ColorModels::setLuvModel(){
    getColorModel = 4;
    luminanceLuvSlider = new QSlider(Qt::Horizontal, this);
    luminanceLuvSlider->setRange(-100,100);
    luminanceLuvSlider->setMinimumWidth(250);

    luminanceLuvSpinBox = new QSpinBox();
    luminanceLuvSpinBox->setRange(-100,100);
    connect(luminanceLuvSlider, SIGNAL(valueChanged(int)), luminanceLuvSpinBox, SLOT(setValue(int)));
    connect(luminanceLuvSpinBox, SIGNAL(valueChanged(int)), luminanceLuvSlider, SLOT(setValue(int)));
    connect(luminanceLuvSlider, SIGNAL(valueChanged(int)), this, SLOT(luvCorectionSlot()));

    uSlider = new QSlider(Qt::Horizontal, this);
    uSlider->setRange(-128,127);
    uSlider->setMinimumWidth(250);

    uSpinBox = new QSpinBox();
    uSpinBox->setRange(-128,127);
    connect(uSlider, SIGNAL(valueChanged(int)), uSpinBox, SLOT(setValue(int)));
    connect(uSpinBox, SIGNAL(valueChanged(int)), uSlider, SLOT(setValue(int)));
    connect(uSlider, SIGNAL(valueChanged(int)), this, SLOT(luvCorectionSlot()));

    vSlider = new QSlider(Qt::Horizontal, this);
    vSlider->setRange(-128,127);
    vSlider->setMinimumWidth(250);

    vSpinBox = new QSpinBox();
    vSpinBox->setRange(-128,127);
    connect(vSlider, SIGNAL(valueChanged(int)), vSpinBox, SLOT(setValue(int)));
    connect(vSpinBox, SIGNAL(valueChanged(int)), vSlider, SLOT(setValue(int)));
    connect(vSlider, SIGNAL(valueChanged(int)), this, SLOT(luvCorectionSlot()));

    layoutColor ->addWidget(new QLabel("L*", this), 0, 0);
    layoutColor ->addWidget(luminanceLuvSlider, 0, 1);
    layoutColor ->addWidget(luminanceLuvSpinBox, 0, 2 );

    layoutColor ->addWidget(new QLabel("u*", this), 1, 0);
    layoutColor ->addWidget(uSlider, 1, 1);
    layoutColor ->addWidget(uSpinBox, 1, 2 );

    layoutColor ->addWidget(new QLabel("v*", this), 2, 0);
    layoutColor ->addWidget(vSlider, 2, 1);
    layoutColor ->addWidget(vSpinBox, 2, 2 );
}

// RGB Model
void ColorModels::rgbInitializeLUT(){
    for(int i=0; i<256; i++){
        redLUT[i]=0;
        greenLUT[i]=0;
        blueLUT[i]=0;
    }
}
void ColorModels::setRgbLUT(int redSliderValue, int greenSliderValue, int blueSliderValue){
    for(int i=0; i<256; i++){
        redLUT[i] = clamp(i + redSliderValue, 0, 255);
        greenLUT[i] = clamp(i + greenSliderValue, 0, 255);
        blueLUT[i] = clamp(i + blueSliderValue, 0, 255);
    }
}
void ColorModels::rgbCorectionSlot(){
    *resultImage = orginalImage;
    int r = redSlider->value();
    int g = greenSlider->value();
    int b = blueSlider->value();
    rgbCorectionLUT(resultImage, r, g, b);
    emit updateImageSignal();
}
void ColorModels::rgbCorectionLUT(QImage* image, int redSliderValue, int greenSliderValue, int blueSliderValue){
    rgbInitializeLUT();
    setRgbLUT(redSliderValue, greenSliderValue, blueSliderValue);
    uchar *p;
    int change = isGray(image);
    for(int y=0; y<image->height(); y++){
        p = image->scanLine(y);
        for(int x=0; x<change*image->width(); x+=change){
           if(change == 1){
                p[x] = redLUT[p[x]];
                p[x] = greenLUT[p[x]];
                p[x] = blueLUT[p[x]];
            }else{
                p[x+2] = redLUT[p[x+2]];
                p[x+1] = greenLUT[p[x+1]];
                p[x] = blueLUT[p[x]];
            }
        }
    }
}

// CMYK Model
void ColorModels::cmykInitialize(){
    for(int i=0; i<4; i++)
        cmyk[i]=0;
}
void ColorModels::cmykCorectionSlot(){
    *resultImage = orginalImage;
    int c = cyjanSlider->value();
    int m = magentaSlider->value();
    int y = yellowSlider->value();
    int k = blackSlider->value();
    cmykCorection(resultImage, c, m, y, k);
    emit updateImageSignal();
}
void ColorModels::cmykCorection(QImage* image, int cyjanSliderValue, int magentaSliderValue, int yellowSliderValue, int blackSliderValue){
    cmykInitialize();
    uchar *p;
    int change = isGray(image);
    for(int y=0; y<image->height(); y++){
        p = image->scanLine(y);
        for(int x=0; x<change*image->width(); x+=change){
               rgb[0] = p[x+2];
               rgb[1] = p[x+1];
               rgb[2] = p[x];

               rgbToCmyk(rgb[0], rgb[1], rgb[2], cmyk[0], cmyk[1], cmyk[2], cmyk[3]);
               cmyk[0] = cmyk[0] + cyjanSliderValue;
               cmyk[1] = cmyk[1] + magentaSliderValue;
               cmyk[2] = cmyk[2] + yellowSliderValue;
               cmyk[3] = cmyk[3] + blackSliderValue;
               cmykToRgb(cmyk[0], cmyk[1], cmyk[2], cmyk[3], rgb[0], rgb[1], rgb[2]);

               p[x+2] = clamp(rgb[0],0,255);
               p[x+1] = clamp(rgb[1],0,255);
                 p[x] = clamp(rgb[2],0,255);
        }
    }
}

// HSL Model
void ColorModels::hslInitialize(){
    for(int i=0; i<3; i++)
        hsl[i]=0.0;
}
void ColorModels::hslCorectionSlot(){
    *resultImage = orginalImage;
    int h = hueSlider->value();
    int s = saturationSlider->value();
    int l = lightnessSlider->value();
    hslCorection(resultImage, h, s, l);
    emit updateImageSignal();
}
void ColorModels::hslCorection(QImage* image, int hueSliderValue, int saturationSliderValue, int lightnessSliderValue){
    hslInitialize();
    uchar *p;
    int change = isGray(image);
    for(int y=0; y<image->height(); y++){
        p = image->scanLine(y);
        for(int x=0; x<change*image->width(); x+=change){
               rgb[0] = p[x+2];
               rgb[1] = p[x+1];
               rgb[2] = p[x];

               rgbToHsl(rgb[0], rgb[1], rgb[2], hsl[0], hsl[1], hsl[2]);
               hsl[0] = hsl[0] + hueSliderValue;
               hsl[1] = clamp(hsl[1] + saturationSliderValue/100.0, 0.0, 1.0);
               hsl[2] = clamp(hsl[2] + lightnessSliderValue/100.0, 0.0, 1.0);
               hslToRgb(hsl[0], hsl[1], hsl[2], rgb[0], rgb[1], rgb[2]);

               p[x+2] = clamp(rgb[0],0,255);
               p[x+1] = clamp(rgb[1],0,255);
                 p[x] = clamp(rgb[2],0,255);
        }
    }
}

// CIE L*a*b* Model
void ColorModels::labInitialize(){
    for(int i=0; i<3; i++)
        lab[i]=0;
}
void ColorModels::labCorectionSlot(){
    *resultImage = orginalImage;
    int l = luminanceLabSlider->value();
    int a = aSlider->value();
    int b = bSlider->value();
    labCorection(resultImage, l, a, b);
    emit updateImageSignal();
}
void ColorModels::labCorection(QImage* image, int luminanceSliderValue, int aSliderValue, int bSliderValue){
    labInitialize();
    uchar *p;
    int change = isGray(image);
    for(int y=0; y<image->height(); y++){
        p = image->scanLine(y);
        for(int x=0; x<change*image->width(); x+=change){
               rgb[0] = p[x+2];
               rgb[1] = p[x+1];
               rgb[2] = p[x];

               rgbToLab(rgb[0], rgb[1], rgb[2], lab[0], lab[1], lab[2]);
               lab[0] = lab[0] + luminanceSliderValue;
               lab[1] = lab[1] + aSliderValue;
               lab[2] = lab[2] + bSliderValue;
               labToRgb(lab[0], lab[1], lab[2], rgb[0], rgb[1], rgb[2]);

               p[x+2] = clamp(rgb[0], 0 ,255);
               p[x+1] = clamp(rgb[1], 0, 255);
                 p[x] = clamp(rgb[2], 0, 255);
        }
    }
}
// CIE L*u*v* Model
void ColorModels::luvInitialize(){
    for(int i=0; i<3; i++)
        luv[i]=0;
}
void ColorModels::luvCorectionSlot(){
    *resultImage = orginalImage;
    int l = luminanceLuvSlider->value();
    int u = uSlider->value();
    int v = vSlider->value();
    luvCorection(resultImage, l, u, v);
    emit updateImageSignal();
}
void ColorModels::luvCorection(QImage* image, int luminanceSliderValue, int uSliderValue, int vSliderValue){
    luvInitialize();
    uchar *p;
    int change = isGray(image);
    for(int y=0; y<image->height(); y++){
        p = image->scanLine(y);
        for(int x=0; x<change*image->width(); x+=change){
               rgb[0] = p[x+2];
               rgb[1] = p[x+1];
               rgb[2] = p[x];

               rgbToLuv(rgb[0], rgb[1], rgb[2], luv[0], luv[1], luv[2]);
               luv[0] = clamp(luv[0] + luminanceSliderValue,0,100);
               luv[1] = clamp(luv[1] + uSliderValue, -134,220);
               luv[2] = clamp(luv[2] + vSliderValue,-140,122);
               luvToRgb(luv[0], luv[1], luv[2], rgb[0], rgb[1], rgb[2]);

               p[x+2] = clamp(rgb[0], 0 ,255);
               p[x+1] = clamp(rgb[1], 0, 255);
               p[x] = clamp(rgb[2], 0, 255);
        }
    }
}
