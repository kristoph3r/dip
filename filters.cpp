#include "filters.h"
#include <QDebug>

Filters::Filters(QWidget *parent) : QWidget(parent)
{
    int maxRadius=10;

    radiusSlider = new QSlider(Qt::Horizontal);
    radiusSlider->setRange(1,maxRadius);
    radiusSlider->setMinimumWidth(100);
    radiusSlider->setValue(1);

    radiusSpinBox = new QSpinBox();
    radiusSpinBox->setRange(1,maxRadius);
    radiusSpinBox->setValue(1);
    connect(radiusSlider, SIGNAL(valueChanged(int)), radiusSpinBox, SLOT(setValue(int)));
    connect(radiusSpinBox, SIGNAL(valueChanged(int)), radiusSlider, SLOT(setValue(int)));

    timerLabel = new QLabel();
    timerLabel->setVisible(false);

    okButton = new QPushButton("OK", this);
    connect(okButton, SIGNAL(clicked()), this, SLOT(okSlot()));

    applyButton = new QPushButton("Apply", this);
    connect(applyButton, SIGNAL(clicked()), this, SLOT(applySlot()));

    cancelButton = new QPushButton("Cancel",this);
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));

    radiusLayout = new QHBoxLayout();
    radiusLayout->addWidget(new QLabel("Radius"));
    radiusLayout->addWidget(radiusSlider);
    radiusLayout->addWidget(radiusSpinBox);

    buttonLayout = new QHBoxLayout();
    buttonLayout->addWidget(cancelButton);
    buttonLayout->addWidget(applyButton);
    buttonLayout->addWidget(okButton);

    timer = new QTime();

}
void Filters::setConvolutionFilter(){
    getFilter = 0;
    maskTable = new QTableWidget();
    fftMode = new QCheckBox("Fast Fourier Transform");
    this->setSizeMaskTable(1);
    connect(radiusSlider, SIGNAL(valueChanged(int)), this, SLOT(setSizeMaskTable(int)));
    connect(maskTable, SIGNAL(cellChanged(int,int)), this, SLOT(resizeCell(int,int)));

    controlLayout = new QVBoxLayout();
    controlLayout->addLayout(radiusLayout);
    controlLayout->addWidget(fftMode);
    controlLayout->addLayout(buttonLayout);
    controlLayout->addWidget(timerLabel);
    controlLayout->setAlignment(Qt::AlignAbsolute);
    controlLayout->setAlignment(Qt::AlignTop);

    convolutionLayout = new QHBoxLayout(this);
    convolutionLayout ->addWidget(maskTable,6);
    convolutionLayout ->addLayout(controlLayout,1);
}
void Filters::setGaussianFilter(){
    getFilter = 1;
    separateGaussMask = new QCheckBox("Optimize");
    gaussianLayout = new QVBoxLayout(this);
    gaussianLayout->addLayout(radiusLayout);
    gaussianLayout->addWidget(separateGaussMask);
    gaussianLayout->addLayout(buttonLayout);
    gaussianLayout->addWidget(timerLabel);
}
void Filters::setUnsharpMaskFilter(){
    getFilter = 2;
    gainSlider = new QSlider(Qt::Horizontal);
    gainSlider->setRange(0,50);
    gainSpinBox = new QSpinBox();
    gainSpinBox->setRange(0,50);
    labMode = new QCheckBox("L*a*b* Mode");
    if(resultImage->isGrayscale())
        labMode->setEnabled(false);
    gainLayout = new QHBoxLayout();
    connect(gainSlider, SIGNAL(valueChanged(int)), gainSpinBox, SLOT(setValue(int)));
    connect(gainSpinBox, SIGNAL(valueChanged(int)), gainSlider, SLOT(setValue(int)));
    gainLayout->addWidget(new QLabel("Gain", this));
    gainLayout->addWidget(gainSlider);
    gainLayout->addWidget(gainSpinBox);
    unsharpLayout = new QVBoxLayout(this);
    unsharpLayout->addLayout(radiusLayout);
    unsharpLayout->addLayout(gainLayout);
    unsharpLayout->addWidget(labMode);
    unsharpLayout->addLayout(buttonLayout);
    unsharpLayout->addWidget(timerLabel);
}
void Filters::setMinimumFilter(){
    getFilter = 3;
    gaussianLayout = new QVBoxLayout(this);
    gaussianLayout->addLayout(radiusLayout);
    gaussianLayout->addLayout(buttonLayout);
    gaussianLayout->addWidget(timerLabel);
}
void Filters::setMaximumFilter(){
    getFilter = 4;
    gaussianLayout = new QVBoxLayout(this);
    gaussianLayout->addLayout(radiusLayout);
    gaussianLayout->addLayout(buttonLayout);
    gaussianLayout->addWidget(timerLabel);
}
void Filters::setMedianFilter(){
    getFilter = 5;
    gaussianLayout = new QVBoxLayout(this);
    gaussianLayout->addLayout(radiusLayout);
    gaussianLayout->addLayout(buttonLayout);
    gaussianLayout->addWidget(timerLabel);
}
void Filters::setAdaptiveMedianFilter(){
    getFilter = 6;
    adaptiveLayout = new QVBoxLayout(this);

    maxRadiusSlider = new QSlider(Qt::Horizontal, this);
    maxRadiusSpinBox = new QSpinBox();
    maxRadiusSlider->setRange(1,10);
    maxRadiusSlider->setMinimumWidth(100);
    maxRadiusSlider->setValue(1);
    maxRadiusSpinBox->setRange(1,10);
    maxRadiusSpinBox->setValue(1);
    connect(maxRadiusSlider, SIGNAL(valueChanged(int)), maxRadiusSpinBox, SLOT(setValue(int)));
    connect(maxRadiusSpinBox, SIGNAL(valueChanged(int)), maxRadiusSlider, SLOT(setValue(int)));
    maxRadiusLayout = new QHBoxLayout();
    maxRadiusLayout->addWidget(new QLabel("Max Radius", this));
    maxRadiusLayout->addWidget(maxRadiusSlider);
    maxRadiusLayout->addWidget(maxRadiusSpinBox);

    adaptiveLayout->addLayout(maxRadiusLayout);
    adaptiveLayout->addLayout(buttonLayout);
    adaptiveLayout->addWidget(timerLabel);
}
void Filters::setImage(QImage *img){
    resultImage = img;
    orginalImage = *resultImage;
}
void Filters::applySlot(){
    timer->start();
    if(getFilter == 0) convolution();
    if(getFilter == 1) gaussian();
    if(getFilter == 2) unsharp();
    if(getFilter == 3) minimum();
    if(getFilter == 4) maximum();
    if(getFilter == 5) median();
    if(getFilter == 6) adaptiveMedian();
    int millisecond = timer->elapsed();
    timerLabel->setText(QString("Filtration time: %1 ms").arg(millisecond));
    timerLabel->setVisible(true);
}
void Filters::okSlot(){
    close();
}
void Filters::cancel(){
    *resultImage = orginalImage;
    close();
    emit updateImageSignal();
}
void Filters::resizeCell(int row, int column){
    maskTable->resizeColumnToContents(column);
    maskTable->resizeRowToContents(row);
}
void Filters::setSizeMaskTable(int radius){
    sizeMask = 2*radius+1;
    maskTable->setRowCount(sizeMask);
    maskTable->setColumnCount(sizeMask);
    maskTable->verticalHeader()->setVisible(false);
    maskTable->horizontalHeader()->setVisible(false);
    maskTableItem = new QTableWidgetItem(QString::number(1));
    maskTableItem->setTextAlignment(Qt::AlignCenter);

    for(int column=0; column<sizeMask; column++){
        for(int row=0; row<sizeMask; row++){
            QTableWidgetItem * element = maskTableItem->clone();
            maskTable->setItem(row,column,element);
        }
    }
    maskTable->resizeColumnsToContents();
    maskTable->resizeRowsToContents();
}
void Filters::createMask(int size){
    mask = new int *[size];
    for(int i=0; i<size; i++)
        mask[i] = new int[size];
}
void Filters::createGaussianMask(int size){
    gaussianMask = new double *[size];
    for(int i=0; i<size; i++)
        gaussianMask[i] = new double[size];
}
void Filters::deleteMask(int size){
    for(int i=0; i<size; i++)
        delete[] mask[i];
    delete [] mask;
}
void Filters::deleteGaussianMask(int size){
    for(int i=0; i<size; i++)
        delete[] gaussianMask[i];
    delete [] gaussianMask;
}
void Filters::setElementMask(int size){
    weightMask=0;
    createMask(size);
    for(int i=0; i<size; i++){
        for(int j=0; j<size; j++){
            mask[i][j] = maskTable->item(i,j)->text().toDouble();
            weightMask +=mask[i][j];
        }
    }
    if(weightMask == 0)
        weightMask=1;
}
void Filters::setElementGaussianMask(int size){
    weightMask = 0;
    createGaussianMask(size);
    double radius = (size-1)/2;
    double stdDev = radius/2.0;
    double variance = stdDev*stdDev;
    double power;
    for(int i=0; i<size; i++){
        for(int j=0; j<size; j++){
            power = (-1.0*((i-radius)*(i-radius)+(j-radius)*(j-radius)))/(2.0*variance);
            gaussianMask[i][j] = pow(M_E,power);
            weightMask +=gaussianMask[i][j];
        }
    }
    if(weightMask == 0.0)
        weightMask=1.0;
}
void Filters::createImageTab(int width, int height){
    redImageTab = new int *[width];
    greenImageTab = new int *[width];
    blueImageTab = new int *[width];
    for(int i=0; i<width; i++){
        redImageTab[i] = new int[height];
        greenImageTab[i] = new int[height];
        blueImageTab[i] = new int[height];
    }
}
void Filters::imageToImageTab(){
    int width = resultImage->width();
    int height = resultImage->height();
    createImageTab(width, height);
    uchar *p;
    for(int y=0; y<height; y++){
        p = resultImage->scanLine(y);
        if(resultImage->isGrayscale()){
            for(int x=0; x<width; x++)
                redImageTab[x][y] = p[x];
        }else{
            for(int x=0; x<4*width; x+=4){
                redImageTab[x/4][y] = p[x+2];
                greenImageTab[x/4][y] = p[x+1];
                blueImageTab[x/4][y] = p[x];
            }
        }
    }
}
void Filters::initializeMedianBucket(){
    for(int i=0; i<256; i++){
        redMedianBucket[i]=0;
        greenMedianBucket[i]=0;
        blueMedianBucket[i]=0;
    }
}
void Filters::convolution(){
    setElementMask(sizeMask);
    imageToImageTab();
    int height = resultImage->height();
    int width = resultImage->width();
    int size = height*width;
    int radius = (sizeMask-1)/2;
    int redConvolutionValue=0;
    int greenConvolutionValue=0;
    int blueConvolutionValue=0;
    if(fftMode->isChecked()){

        maskImage = new QImage(width, height, QImage::Format_RGB32);

        int widthMarginMask = (width-sizeMask)/2;
        int heightMarginMask = (height-sizeMask)/2;
        for(int y=0; y<height; y++){
            for(int x=0; x<width; x++){
                redImageTab[x][y] = 0;
            }
        }
        for(int y=heightMarginMask; y<heightMarginMask+sizeMask; y++){
            for(int x=widthMarginMask; x<widthMarginMask+sizeMask; x++){
                redImageTab[x][y] = mask[x-widthMarginMask][y-heightMarginMask];
            }
        }
        fftw_complex *inRed, *inGreen, *inBlue;
        fftw_complex *outRed, *outGreen, *outBlue;
        fftw_complex *inMask, *outMask;

        inRed = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
        inGreen = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
        inBlue = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
        inMask = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
        outRed = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
        outGreen = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
        outBlue = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
        outMask = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);

        QRgb *p = (QRgb*)resultImage->bits();
        QRgb *q = (QRgb*)maskImage->bits();

        for(int i=0; i<size; i++){
            q[i] = qRgb(redImageTab[i/width][i%width], 0, 0);
        }

        replaceParts(maskImage);
        for(int i=0; i<size; i++){
            inRed[i][0] = qRed(p[i]);
            inRed[i][1] = 0;
            inGreen[i][0] = qGreen(p[i]);
            inGreen[i][1] = 0;
            inBlue[i][0] = qBlue(p[i]);
            inBlue[i][1] = 0;
            inMask[i][0] = (qRed(q[i])/weightMask);
            inMask[i][1] = 0;
        }
        fftw_plan plan;
        plan = fftw_plan_dft_2d(height, width, inRed, outRed, FFTW_FORWARD, FFTW_ESTIMATE);
        fftw_execute(plan);
        fftw_destroy_plan(plan);

        plan = fftw_plan_dft_2d(height, width, inGreen, outGreen, FFTW_FORWARD, FFTW_ESTIMATE);
        fftw_execute(plan);
        fftw_destroy_plan(plan);

        plan = fftw_plan_dft_2d(height, width, inBlue, outBlue, FFTW_FORWARD, FFTW_ESTIMATE);
        fftw_execute(plan);
        fftw_destroy_plan(plan);

        plan = fftw_plan_dft_2d(height, width, inMask, outMask, FFTW_FORWARD, FFTW_ESTIMATE);
        fftw_execute(plan);
        fftw_destroy_plan(plan);

        // ......................
       for(int i=0; i<size; i++){

            inRed[i][0] = (outRed[i][0]*outMask[i][0]) - (outRed[i][1]*outMask[i][1]);
            inRed[i][1] = (outRed[i][1]*outMask[i][0]) + (outRed[i][0]*outMask[i][1]);
            inGreen[i][0] = (outGreen[i][0]*outMask[i][0]) - (outGreen[i][1]*outMask[i][1]);
            inGreen[i][1] = (outGreen[i][1]*outMask[i][0]) + (outGreen[i][0]*outMask[i][1]);
            inBlue[i][0] = (outBlue[i][0]*outMask[i][0]) - (outBlue[i][1]*outMask[i][1]);
            inBlue[i][1] = (outBlue[i][1]*outMask[i][0]) + (outBlue[i][0]*outMask[i][1]);
        }

      //........................
        plan = fftw_plan_dft_2d(height, width, inRed, outRed, FFTW_BACKWARD, FFTW_ESTIMATE);
        fftw_execute(plan);
        fftw_destroy_plan(plan);

        plan = fftw_plan_dft_2d(height, width, inGreen, outGreen, FFTW_BACKWARD, FFTW_ESTIMATE);
        fftw_execute(plan);
        fftw_destroy_plan(plan);

        plan = fftw_plan_dft_2d(height, width, inBlue, outBlue, FFTW_BACKWARD, FFTW_ESTIMATE);
        fftw_execute(plan);
        fftw_destroy_plan(plan);

        for(int i=0; i<size; i++){
            p[i+width+1] = qRgb(outRed[i][0]/size, outGreen[i][0]/size, outBlue[i][0]/size);
        }

        fftw_free(inRed);
        fftw_free(outRed);
        fftw_free(inGreen);
        fftw_free(outGreen);
        fftw_free(inBlue);
        fftw_free(outBlue);
        fftw_free(inMask);
        fftw_free(outMask);
    }else{
        uchar *p;
        for(int y=0; y<height; y++){
            p = resultImage->scanLine(y);
            for(int x=0; x<width; x++){
                redConvolutionValue=0;
                greenConvolutionValue=0;
                blueConvolutionValue=0;
                for(int j=0; j<sizeMask; j++){
                    for(int i=0; i<sizeMask; i++){
                        if((x+i-radius)<0 || (x+i-radius)>=width || (y+j-radius)<0 || (y+j-radius)>=height){
                            redConvolutionValue +=0;
                            greenConvolutionValue +=0;
                            blueConvolutionValue +=0;
                        }else{
                            redConvolutionValue +=mask[i][j]*redImageTab[x+i-radius][y+j-radius];
                            greenConvolutionValue +=mask[i][j]*greenImageTab[x+i-radius][y+j-radius];
                            blueConvolutionValue +=mask[i][j]*blueImageTab[x+i-radius][y+j-radius];
                        }
                     }
                }
                if(resultImage->isGrayscale())
                    p[x] = clamp(qRound(redConvolutionValue/weightMask),0,255);
                else{
                    p[4*x+2] = clamp(qRound(redConvolutionValue/weightMask),0,255);
                    p[4*x+1] = clamp(qRound(greenConvolutionValue/weightMask),0,255);
                    p[4*x] = clamp(qRound(blueConvolutionValue/weightMask),0,255);
                }
            }
        }
    }
    emit updateImageSignal();
    deleteMask(sizeMask);
}
void Filters::gaussian(){
    int radius = radiusSlider->value();
    sizeMask = 2*radius+1;
    setElementGaussianMask(sizeMask);
    imageToImageTab();
    int height = resultImage->height();
    int width = resultImage->width();
    double redGaussianValue;
    double greenGaussianValue;
    double blueGaussianValue;
    if(separateGaussMask->isChecked()){
        int red,green,blue;
        weightMask = 0;
        double separateMask[sizeMask];
        for(int i=0; i<sizeMask; i++){
            separateMask[i] = gaussianMask[radius][i];
            weightMask += separateMask[i];
        }
        QRgb *p;
        for(int y=0; y<height; y++){
            p = (QRgb*)resultImage->scanLine(y);
            for(int x=0; x<width; x++){
                redGaussianValue=0;
                greenGaussianValue=0;
                blueGaussianValue=0;
                for(int i=0; i<sizeMask; i++){
                    if((x+i-radius)<0 || (x+i-radius)>=width){
                        redGaussianValue += 0.0;
                        greenGaussianValue += 0.0;
                        blueGaussianValue += 0.0;
                    }else{
                        redGaussianValue += qRed(p[x+i-radius])*separateMask[i];
                        greenGaussianValue += qGreen(p[x+i-radius])*separateMask[i];
                        blueGaussianValue += qBlue(p[x+i-radius])*separateMask[i];
                    }
                }
                red = clamp(qRound(redGaussianValue/weightMask),0,255);
                green = clamp(qRound(greenGaussianValue/weightMask),0,255);
                blue = clamp(qRound(blueGaussianValue/weightMask),0,255);
                p[x] = qRgb(red, green, blue);
            }
        }
        imageToImageTab();
        for(int y=0; y<height; y++){
            p = (QRgb*)resultImage->scanLine(y);
            for(int x=0; x<width; x++){
                redGaussianValue=0;
                greenGaussianValue=0;
                blueGaussianValue=0;
                for(int i=0; i<sizeMask; i++){
                    if((y+i-radius)<0 || (y+i-radius)>=height){
                        redGaussianValue += 0.0;
                        greenGaussianValue += 0.0;
                        blueGaussianValue += 0.0;
                    }else{
                        redGaussianValue += redImageTab[x][y+i-radius]*separateMask[i];
                        greenGaussianValue += greenImageTab[x][y+i-radius]*separateMask[i];
                        blueGaussianValue +=  blueImageTab[x][y+i-radius]*separateMask[i];
                    }
                }
                red = clamp(qRound(redGaussianValue/weightMask),0,255);
                green = clamp(qRound(greenGaussianValue/weightMask),0,255);
                blue = clamp(qRound(blueGaussianValue/weightMask),0,255);
                p[x] = qRgb(red, green, blue);
            }
        }
    }else{
        uchar *p;
        for(int y=0; y<height; y++){
            p = resultImage->scanLine(y);
            for(int x=0; x<width; x++){
                redGaussianValue=0;
                greenGaussianValue=0;
                blueGaussianValue=0;
                for(int j=0; j<sizeMask; j++){
                    for(int i=0; i<sizeMask; i++){
                        if((x+i-radius)<0 || (x+i-radius)>=width || (y+j-radius)<0 || (y+j-radius)>=height){
                            redGaussianValue += 0.0;
                            greenGaussianValue += 0.0;
                            blueGaussianValue += 0.0;
                        }else{
                            redGaussianValue +=gaussianMask[i][j]*redImageTab[x+i-radius][y+j-radius];
                            greenGaussianValue +=gaussianMask[i][j]*greenImageTab[x+i-radius][y+j-radius];
                            blueGaussianValue +=gaussianMask[i][j]*blueImageTab[x+i-radius][y+j-radius];
                        }
                     }
                }
                if(resultImage->isGrayscale())
                    p[x] = clamp(qRound(redGaussianValue/weightMask),0,255);
                else{
                    p[4*x+2] = clamp(qRound(redGaussianValue/weightMask),0,255);
                    p[4*x+1] = clamp(qRound(greenGaussianValue/weightMask),0,255);
                    p[4*x] = clamp(qRound(blueGaussianValue/weightMask),0,255);
                }
            }
        }
    }
    emit updateImageSignal();
    deleteGaussianMask(sizeMask);
}
void Filters::unsharp(){
    int orginalL, orginalA, orginalB;
    int l,a,b;
    int red, green, blue;
    int alpha = gainSlider->value();
    int height = resultImage->height();
    int width = resultImage->width();
    gaussian();
    uchar *orginal;
    uchar *gmask;
    for(int y=0; y<height; y++){
        orginal = orginalImage.scanLine(y);
        gmask = resultImage->scanLine(y);
        for(int x=0; x<width; x++){
            if(labMode->isChecked()){
                rgbToLab(orginal[4*x+2], orginal[4*x+1], orginal[4*x], orginalL, orginalA, orginalB);
                rgbToLab(gmask[4*x+2], gmask[4*x+1], gmask[4*x], l, a, b);
                l = orginalL+alpha*(orginalL-l);
                labToRgb(l,orginalA, orginalB, red, green, blue);
                gmask[4*x+2] = clamp(red,0,255);
                gmask[4*x+1] = clamp(green,0,255);
                gmask[4*x] = clamp(blue,0,255);
            }else{
                if(resultImage->isGrayscale())
                    gmask[x] = clamp(orginal[x]+alpha*(orginal[x]-gmask[x]),0,255);
                else{
                    gmask[4*x+2] = clamp(orginal[4*x+2]+alpha*(orginal[4*x+2]-gmask[4*x+2]),0,255);
                    gmask[4*x+1] = clamp(orginal[4*x+1]+alpha*(orginal[4*x+1]-gmask[4*x+1]),0,255);
                    gmask[4*x] = clamp(orginal[4*x]+alpha*(orginal[4*x]-gmask[4*x]),0,255);
                }
            }
        }
    }
    emit updateImageSignal();
}
void Filters::minimum(){
    int radius = radiusSlider->value();
    sizeMask = 2*radius+1;
    imageToImageTab();
    int height = resultImage->height();
    int width = resultImage->width();
    int redMinimumValue;
    int greenMinimumValue;
    int blueMinimumValue;
    uchar *p;
    for(int y=0; y<height; y++){
        p = resultImage->scanLine(y);
        for(int x=0; x<width; x++){
            redMinimumValue=255;
            greenMinimumValue=255;
            blueMinimumValue=255;
            for(int j=0; j<sizeMask; j++){
                for(int i=0; i<sizeMask; i++){
                    if(!((x+i-radius)<0 || (x+i-radius)>=width || (y+j-radius)<0 || (y+j-radius)>=height)){
                        if(redImageTab[x+i-radius][y+j-radius] < redMinimumValue)
                            redMinimumValue = redImageTab[x+i-radius][y+j-radius];
                        if(greenImageTab[x+i-radius][y+j-radius] < greenMinimumValue)
                            greenMinimumValue = greenImageTab[x+i-radius][y+j-radius];
                        if(blueImageTab[x+i-radius][y+j-radius] < blueMinimumValue)
                            blueMinimumValue = blueImageTab[x+i-radius][y+j-radius];
                    }
                 }
            }
            if(resultImage->isGrayscale())
                p[x] = clamp(redMinimumValue,0,255);
            else{
                p[4*x+2] = clamp(redMinimumValue,0,255);
                p[4*x+1] = clamp(greenMinimumValue,0,255);
                p[4*x] = clamp(blueMinimumValue,0,255);
            }
        }
    }
    emit updateImageSignal();
}
void Filters::maximum(){
    int radius = radiusSlider->value();
    sizeMask = 2*radius+1;
    imageToImageTab();
    int height = resultImage->height();
    int width = resultImage->width();
    int redMaximumValue;
    int greenMaximumValue;
    int blueMaximumValue;
    uchar *p;
    for(int y=0; y<height; y++){
        p = resultImage->scanLine(y);
        for(int x=0; x<width; x++){
            redMaximumValue=0;
            greenMaximumValue=0;
            blueMaximumValue=0;
            for(int j=0; j<sizeMask; j++){
                for(int i=0; i<sizeMask; i++){
                    if(!((x+i-radius)<0 || (x+i-radius)>=width || (y+j-radius)<0 || (y+j-radius)>=height)){
                        if(redImageTab[x+i-radius][y+j-radius] > redMaximumValue)
                            redMaximumValue = redImageTab[x+i-radius][y+j-radius];
                        if(greenImageTab[x+i-radius][y+j-radius] > greenMaximumValue)
                            greenMaximumValue = greenImageTab[x+i-radius][y+j-radius];
                        if(blueImageTab[x+i-radius][y+j-radius] > blueMaximumValue)
                            blueMaximumValue = blueImageTab[x+i-radius][y+j-radius];
                    }
                 }
            }
            if(resultImage->isGrayscale())
                p[x] = clamp(redMaximumValue,0,255);
            else{
                p[4*x+2] = clamp(redMaximumValue,0,255);
                p[4*x+1] = clamp(greenMaximumValue,0,255);
                p[4*x] = clamp(blueMaximumValue,0,255);
            }
        }
    }
    emit updateImageSignal();
}
void Filters::median(){
    int radius = radiusSlider->value();
    sizeMask = 2*radius+1;
    int maskElements = sizeMask*sizeMask;
    imageToImageTab();
    int height = resultImage->height();
    int width = resultImage->width();
    int sumR = 0;
    int sumG = 0;
    int sumB = 0;
    int medianaR = 0;
    int medianaG = 0;
    int medianaB = 0;
    uchar *p;
    if(resultImage->isGrayscale()){
        for(int y=0; y<height; y++){
            p = resultImage->scanLine(y);
            for(int x=0; x<width; x++){
                initializeMedianBucket();
                for(int j=0; j<sizeMask; j++){
                    for(int i=0; i<sizeMask; i++){
                        if(!((x+i-radius)<0 || (x+i-radius)>=width || (y+j-radius)<0 || (y+j-radius)>=height))
                            redMedianBucket[redImageTab[x+i-radius][y+j-radius]]++;
                    }
                }
                sumR = redMedianBucket[0];
                for(int m=0; m<256; m++){
                    if(sumR < maskElements/2){
                        sumR += redMedianBucket[m];
                        medianaR = m;
                    }
                }
                p[x] = medianaR;
            }
        }
    }else{
        for(int y=0; y<height; y++){
            p = resultImage->scanLine(y);
            for(int x=0; x<width; x++){
                initializeMedianBucket();
                for(int j=0; j<sizeMask; j++){
                    for(int i=0; i<sizeMask; i++){
                        if(!((x+i-radius)<0 || (x+i-radius)>=width || (y+j-radius)<0 || (y+j-radius)>=height)){
                            redMedianBucket[redImageTab[x+i-radius][y+j-radius]]++;
                            greenMedianBucket[greenImageTab[x+i-radius][y+j-radius]]++;
                            blueMedianBucket[blueImageTab[x+i-radius][y+j-radius]]++;
                        }
                    }
                }
                sumR = redMedianBucket[0];
                sumG = greenMedianBucket[0];
                sumB = blueMedianBucket[0];
                for(int m=0; m<256; m++){
                    if(sumR < maskElements/2){
                        sumR += redMedianBucket[m];
                        medianaR = m;
                    }if(sumG < maskElements/2){
                        sumG += greenMedianBucket[m];
                        medianaG = m;
                    }if(sumB < maskElements/2){
                        sumB += blueMedianBucket[m];
                        medianaB = m;
                    }
                }
                p[4*x+2] = medianaR;
                p[4*x+1] = medianaG;
                p[4*x] = medianaB;
            }
        }
    }
    emit updateImageSignal();
}
void Filters::adaptiveMedian(){
    int red, green, blue;
    int maxRadius = maxRadiusSlider->value();
    int radius = 1;
    sizeMask = 2*radius+1;
    int maskElements = sizeMask*sizeMask;
    int height = resultImage->height();
    int width = resultImage->width();

    int sumR = 0;
    int sumG = 0;
    int sumB = 0;
    int medianaR = 0;
    int medianaG = 0;
    int medianaB = 0;
    int redMinimumValue;
    int greenMinimumValue;
    int blueMinimumValue;
    int redMaximumValue;
    int greenMaximumValue;
    int blueMaximumValue;
    createImageTab(width, height);
    uchar *p;
    for(int y=0; y<height; y++){
        p = resultImage->scanLine(y);
        for(int x=0; x<4*width; x+=4){
            redImageTab[x/4][y] = p[x+2];
            greenImageTab[x/4][y] = p[x+1];
            blueImageTab[x/4][y] = p[x];
        }
    }

    for(int y=0; y<height; y++){
        p = resultImage->scanLine(y);
        for(int x=0; x<width; x++){
            radius = 1;
            sizeMask = 2*radius+1;
            maskElements = sizeMask*sizeMask;

            for(radius=1; radius<=maxRadius; radius++){
                redMinimumValue=255;
                greenMinimumValue=255;
                blueMinimumValue=255;
                redMaximumValue, greenMaximumValue=0;
                blueMaximumValue=0;
                sizeMask = 2*radius+1;
                maskElements = sizeMask*sizeMask;
                initializeMedianBucket();

                for(int j=0; j<sizeMask; j++){
                    for(int i=0; i<sizeMask; i++){
                        if(!((x+i-radius)<0 || (x+i-radius)>=width || (y+j-radius)<0 || (y+j-radius)>=height)){
                            redMedianBucket[redImageTab[x+i-radius][y+j-radius]]++;
                            greenMedianBucket[greenImageTab[x+i-radius][y+j-radius]]++;
                            blueMedianBucket[blueImageTab[x+i-radius][y+j-radius]]++;

                            //minimum
                            if(redImageTab[x+i-radius][y+j-radius] < redMinimumValue)
                                redMinimumValue = redImageTab[x+i-radius][y+j-radius];
                            if(greenImageTab[x+i-radius][y+j-radius] < greenMinimumValue)
                                greenMinimumValue = greenImageTab[x+i-radius][y+j-radius];
                            if(blueImageTab[x+i-radius][y+j-radius] < blueMinimumValue)
                                blueMinimumValue = blueImageTab[x+i-radius][y+j-radius];

                            //maximum
                            if(redImageTab[x+i-radius][y+j-radius] > redMaximumValue)
                                redMaximumValue = redImageTab[x+i-radius][y+j-radius];
                            if(greenImageTab[x+i-radius][y+j-radius] > greenMaximumValue)
                                greenMaximumValue = greenImageTab[x+i-radius][y+j-radius];
                            if(blueImageTab[x+i-radius][y+j-radius] > blueMaximumValue)
                                blueMaximumValue = blueImageTab[x+i-radius][y+j-radius];
                        }
                     }
                }
                sumR = redMedianBucket[0];
                sumG = greenMedianBucket[0];
                sumB = blueMedianBucket[0];
                for(int m=0; m<256; m++){
                    if(sumR < maskElements/2){
                        sumR += redMedianBucket[m];
                        medianaR = m;
                    }if(sumG < maskElements/2){
                        sumG += greenMedianBucket[m];
                        medianaG = m;
                    }if(sumB < maskElements/2){
                        sumB += blueMedianBucket[m];
                        medianaB = m;
                    }
                }
                //red
                if((medianaR-redMinimumValue)>0 && (medianaR-redMaximumValue)<0){
                    if((redImageTab[x][y]-redMinimumValue)>0 && (redImageTab[x][y]-redMaximumValue)<0){
                        red = redImageTab[x][y];
                    }else{
                        red = medianaR;
                    }
                }else{
                    continue;
                }
                //green
                if((medianaG-greenMinimumValue)>0 && (medianaG-greenMaximumValue)<0){
                    if((greenImageTab[x][y]-greenMinimumValue)>0 && (greenImageTab[x][y]-greenMaximumValue)<0){
                        green = greenImageTab[x][y];
                    }else{
                        green = medianaG;
                    }
                }else{
                    continue;
                }
                //blue
                if((medianaB-blueMinimumValue)>0 && (medianaB-blueMaximumValue)<0){
                    if((blueImageTab[x][y]-blueMinimumValue)>0 && (blueImageTab[x][y]-blueMaximumValue)<0){
                        blue = blueImageTab[x][y];
                    }else{
                        blue = medianaB;
                    }
                }else{
                    continue;
                }
                break;
            }
            p[4*x+2] = red;
            p[4*x+1] = green;
            p[4*x] = blue;
        }
    }
    emit updateImageSignal();
}
