#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include "imagewidget.h"
#include "math.h"
#include <QWidget>
#include <QPainter>
#include <QComboBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QRadioButton>
#include <QSpinBox>

class Histogram : public QWidget
{

    Q_OBJECT

    QWidget *infoHistWidget;
    QComboBox *colorChangeBox;
    QRadioButton *inputLevelsRadio;
    QRadioButton *outputLevelsRadio;
    QSpinBox *leftSpinBox;
    QSpinBox *rightSpinBox;
    QPushButton *equalizationButton;   
    QPushButton *resetButton;
    QPushButton *okButton;
    QPushButton *cancelButton;
    QLabel *averageIntensity;
    QLabel *varianceIntensity;
    QLabel *standardDeviation;
    QLabel *pixels;    
    QVBoxLayout *radioLayout;
    QHBoxLayout *spinBoxLayout;
    QGridLayout *buttonLayout;
    QVBoxLayout *infoHistLayout;

    QImage *histImage;
    QImage orginalImage;
    int redHistArray[256];
    int greenHistArray[256];
    int blueHistArray[256];
    int valueHistArray[256];
    int cumulativeRedHistArray[256];
    int cumulativeGreenHistArray[256];
    int cumulativeBlueHistArray[256];
    int redLUT[256];
    int greenLUT[256];
    int blueLUT[256];
    int colorHist;
    float pixelsInImage;

    void initializeHistArray();
    void calculateHistogram(QImage*);
    void calculateValueHistogram();
    float calculateAvgIntensity(int);
    float calculateVarianceIntensity(int);
    void createCumulativeHistogram();
    int strechingLevels(int, int, int);
    int maxValue(int []);
    int maxValue(double []);
    int isGray(QImage *);
    int clamp(int, int, int);

public:
     Histogram(QWidget *parent = 0);
     void setImage(QImage*);
     void paintEvent(QPaintEvent*);
     
signals:
    void updateImageSignal();

public slots:
     void drawHist(int);
     void equalization();
     void reset();
     void cancel();
     void strechingSlot();

};

#endif // HISTOGRAM_H
