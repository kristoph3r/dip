#include "mainwindow.h"


MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent)
{
    generalWidget = new QWidget(this);
    generalLayout = new QHBoxLayout();
    imageWidget = new ImageWidget();
    generalLayout->setContentsMargins(0,0,0,0);
    generalLayout->setMargin(1);
    generalLayout->setSpacing(0);
    generalLayout->addWidget(imageWidget);
    generalWidget->setLayout(generalLayout);
    setCentralWidget(generalWidget);
    createMenu();
    setTitle();
}
void MainWindow::createMenu(){
    fileMenu = menuBar()->addMenu("File");
    fileMenu ->addAction("Open", this, SLOT(openFile()));
    fileMenu ->addAction("Close", this, SLOT(closeFile()));
    fileMenu ->addSeparator();
    //fileMenu ->addAction("Exit", qApp, SLOT(quit()));

    intensityMenu = menuBar()->addMenu("Intensity");
    intensityMenu ->addAction("Brightness, Contrast, Gamma", this, SLOT(bgcCorrection()));
    intensityMenu ->addAction("Histogram", this, SLOT(viewHistogram()));
    intensityMenu ->setEnabled(false);

    colorMenu = menuBar()->addMenu("Colors");
    colorMenu ->addAction("RGB", this, SLOT(rgbColorModel()));
    colorMenu ->addAction("CMYK", this, SLOT(cmykColorModel()));
    colorMenu ->addAction("HSL", this, SLOT(hslColorModel()));
    colorMenu ->addAction("CIE L*a*b*", this, SLOT(labColorModel()));
    colorMenu ->addAction("CIE L*u*v*", this, SLOT(luvColorModel()));
    colorMenu ->setEnabled(false);

    filtersMenu = menuBar()->addMenu("Filters");
    filtersMenu ->addAction("Convolution", this, SLOT(convolutionTable()));
    filtersMenu ->addAction("Gaussian Filter", this, SLOT(gaussianFilter()));
    filtersMenu ->addAction("Unsharp Mask", this, SLOT(unsharpMaskFilter()));
    filtersMenu ->addAction("Minimum Filter", this, SLOT(minimumFilter()));
    filtersMenu ->addAction("Maximum Filter", this, SLOT(maximumFilter()));
    filtersMenu ->addAction("Median Filter", this, SLOT(medianFilter()));
    filtersMenu ->addAction("Adaptive Median", this, SLOT(adaptiveMedianFilter()));
    filtersMenu ->setEnabled(false);

    fourierMenu = menuBar()->addMenu("Transform");
    fourierMenu ->addAction("View Fourier", this, SLOT(viewFourierTransform()));
    fourierMenu ->addSeparator();
    fourierMenu ->addAction("Add Watermark", this, SLOT(addWatermarkSlot()));
    fourierMenu ->addAction("Check Watermark", this, SLOT(checkWatermarkSlot()));
    fourierMenu ->setEnabled(false);

    save = new QAction("Compression", this);
    connect(save, SIGNAL(triggered()), this, SLOT(saveCompression()));
    load = new QAction("Decompression", this);
    connect(load, SIGNAL(triggered()), this, SLOT(loadCompression()));
    compressionMenu = menuBar()->addMenu("Compression");
    compressionMenu ->addAction(save);
    compressionMenu ->addAction(load);
    save ->setEnabled(false);

    aboutMenu = menuBar()->addMenu("About");
}
void MainWindow::setTitle(){
    setWindowTitle("DIP: Kraus Krzysztof");
}
void MainWindow::setEnabledMenu(){
    intensityMenu ->setEnabled(true);
    colorMenu ->setEnabled(true);
    filtersMenu ->setEnabled(true);
    fourierMenu ->setEnabled(true);
    save ->setEnabled(true);
}

void MainWindow::openFile(){
    fileName = QFileDialog::getOpenFileName(this, "Open Image", "/Users/kristoph3r/Desktop", "Image Files (*.png *.jpg *.bmp)");
  //  fileName = "/Users/kristoph3r/Desktop/lena.bmp";                //test
    if(!fileName.isNull() && !fileName.isEmpty()){
        setWindowTitle(tr("DIP: %1").arg(fileName));
        QImage image(fileName);
        if(!image.isNull()){
            if(image.width()>1024 ||image.height()>768){
                image = image.scaled(QSize(1024,768), Qt::KeepAspectRatio, Qt::FastTransformation);
            }
            imageWidget->setFixedSize(image.size());
            this->setFixedSize(image.width()+2*generalLayout->margin(),image.height()+2*generalLayout->margin());
            imageWidget->setImage(image);
            setEnabledMenu();
            if(image.isGrayscale())
                colorMenu->setEnabled(false);

        }
    }
}
void MainWindow::closeFile(){
    intensityMenu ->setEnabled(false);
    colorMenu->setEnabled(false);
    filtersMenu->setEnabled(false);
    fourierMenu->setEnabled(false);
    save ->setEnabled(false);
    imageWidget ->removeImage();
    this ->setFixedSize(1024,768);
    this ->setTitle();
}
void MainWindow::bgcCorrection(){
    bgcWindow = new Bgc();
    connect(bgcWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    bgcWindow->setImage(imageWidget->image());
    bgcWindow->show();
}
void MainWindow::viewHistogram(){
    histogramWindow = new Histogram();
    histogramWindow->setImage(imageWidget->image());
    connect(histogramWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    connect(imageWidget, SIGNAL(repaintSignal()), histogramWindow, SLOT(update()));
    histogramWindow->setFixedSize(600,350);
    histogramWindow->show();
}
void MainWindow::rgbColorModel(){
    colorModelWindow  = new ColorModels();
    colorModelWindow->setImage(imageWidget->image());
    connect(colorModelWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    colorModelWindow->setRgbModel();
    colorModelWindow->show();
}
void MainWindow::cmykColorModel(){
    colorModelWindow  = new ColorModels();
    colorModelWindow->setImage(imageWidget->image());
    connect(colorModelWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    colorModelWindow ->setCmykModel();
    colorModelWindow->show();
}
void MainWindow::hslColorModel(){
    colorModelWindow  = new ColorModels();
    colorModelWindow->setImage(imageWidget->image());
    connect(colorModelWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    colorModelWindow ->setHslModel();
    colorModelWindow->show();
}
void MainWindow::labColorModel(){
    colorModelWindow  = new ColorModels();
    colorModelWindow->setImage(imageWidget->image());
    connect(colorModelWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    colorModelWindow->setLabModel();
    colorModelWindow->show();
}
void MainWindow::luvColorModel(){
    colorModelWindow  = new ColorModels();
    colorModelWindow->setImage(imageWidget->image());
    connect(colorModelWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    colorModelWindow->setLuvModel();
    colorModelWindow->show();
}
void MainWindow::convolutionTable(){
    filterWindow = new Filters();
    filterWindow->setWindowTitle("Convolution");
    filterWindow->setImage(imageWidget->image());
    filterWindow->setFixedSize(818,586);
    connect(filterWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    filterWindow->setConvolutionFilter();
    filterWindow->show();
}
void MainWindow::gaussianFilter(){
    filterWindow = new Filters();
    filterWindow->setWindowTitle("Gaussian");
    filterWindow->setImage(imageWidget->image());
    connect(filterWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    filterWindow->setFixedWidth(400);
    filterWindow->setMinimumHeight(200);
    filterWindow->setGaussianFilter();
    filterWindow->show();
}
void MainWindow::unsharpMaskFilter(){
    filterWindow = new Filters();
    filterWindow->setWindowTitle("Unsharp Mask");
    filterWindow->setImage(imageWidget->image());
    connect(filterWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    filterWindow->setFixedWidth(400);
    filterWindow->setMinimumHeight(200);
    filterWindow->setUnsharpMaskFilter();
    filterWindow->show();
}
void MainWindow::minimumFilter(){
    filterWindow = new Filters();
    filterWindow->setWindowTitle("Minimum");
    filterWindow->setImage(imageWidget->image());
    connect(filterWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    filterWindow->setFixedWidth(400);
    filterWindow->setMinimumHeight(200);
    filterWindow->setMinimumFilter();
    filterWindow->show();
}
void MainWindow::maximumFilter(){
    filterWindow = new Filters();
    filterWindow->setWindowTitle("Maximum");
    filterWindow->setImage(imageWidget->image());
    connect(filterWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    filterWindow->setFixedWidth(400);
    filterWindow->setMinimumHeight(200);
    filterWindow->setMaximumFilter();
    filterWindow->show();
}
void MainWindow::medianFilter(){
    filterWindow = new Filters();
    filterWindow->setWindowTitle("Median");
    filterWindow->setImage(imageWidget->image());
    connect(filterWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    filterWindow->setFixedWidth(400);
    filterWindow->setMinimumHeight(200);
    filterWindow->setMedianFilter();
    filterWindow->show();
}
void MainWindow::adaptiveMedianFilter(){
    filterWindow = new Filters();
    filterWindow->setWindowTitle("Adaptive Median");
    filterWindow->setImage(imageWidget->image());
    connect(filterWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    filterWindow->setFixedWidth(400);
    filterWindow->setMinimumHeight(200);
    filterWindow->setAdaptiveMedianFilter();
    filterWindow->show();
}
void MainWindow::viewFourierTransform(){
    fourierWindow = new FourierTransform();
    fourierWindow->setWindowTitle("Fourier Transform");
    fourierWindow->setImage(imageWidget->image());
    connect(fourierWindow, SIGNAL(updateImageSignal()), imageWidget, SLOT(update()));
    fourierWindow->show();
}
void MainWindow::saveCompression(){
    compressObj = new Compression();
    compressObj->setImage(imageWidget->image());
    compressObj->show();
}
void MainWindow::loadCompression(){
    compressObj = new Compression();
    QImage img = compressObj->decompress();
    imageWidget->setImage(img);
    this->setFixedSize(img.width()+2*generalLayout->margin(),img.height()+2*generalLayout->margin());
    this->setWindowTitle(compressObj->getPath());
    setEnabledMenu();
}
void MainWindow::addWatermarkSlot(){
    waterMark = new Watermarking();
    waterMark->setWindowTitle("add Watermark");
    waterMark->setImage(imageWidget->image());
    waterMark->addWatermarkInterface(waterMark->getSizeImage());
    waterMark->show();
}
void MainWindow::checkWatermarkSlot(){
    waterMark = new Watermarking();
    waterMark->setImage(imageWidget->image());
    waterMark->checkWatermark();
}
