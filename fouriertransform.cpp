#include "fouriertransform.h"

FourierTransform::FourierTransform(QWidget *parent) : QWidget(parent)
{
    imageRadio = new QRadioButton("Orginal Image");
    module = new QRadioButton("Module");
    phase = new QRadioButton("Phase");
    realis = new QRadioButton("Realis");
    imaginalis = new QRadioButton("imaginalis");
    connect(imageRadio, SIGNAL(clicked()), this, SLOT(okSlot()));
    connect(module, SIGNAL(clicked()), this, SLOT(okSlot()));
    connect(phase, SIGNAL(clicked()), this, SLOT(okSlot()));
    connect(realis, SIGNAL(clicked()), this, SLOT(okSlot()));
    connect(imaginalis, SIGNAL(clicked()), this, SLOT(okSlot()));

    okButton = new QPushButton("OK", this);
    connect(okButton, SIGNAL(clicked()), this, SLOT(close()));

    cancelButton = new QPushButton("Cancel", this);
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));

    radioLayout = new QVBoxLayout();
    buttonLayout = new QHBoxLayout();
    fourierLayout = new QVBoxLayout(this);

    radioLayout->addWidget(imageRadio);
    radioLayout->addSpacing(20);
    radioLayout->addWidget(module);
    radioLayout->addWidget(phase);
    radioLayout->addWidget(realis);
    radioLayout->addWidget(imaginalis);

    buttonLayout->addWidget(cancelButton);
    buttonLayout->addWidget(okButton);

    fourierLayout->addLayout(radioLayout);
    fourierLayout->addLayout(buttonLayout);
}

void FourierTransform::setImage(QImage *img){
    resultImage = new QImage(img->width(), img->height(), QImage::Format_RGB32);
    resultImage = img;
    orginalImage = *resultImage;
}
void FourierTransform::fourierModule(){
    int width = resultImage->width();
    int height = resultImage->height();
    int size =width*height;
    int red,green,blue;
    double moduleRed[size], moduleGreen[size], moduleBlue[size];
    double maxRed = 0.0;
    double maxGreen = 0.0;
    double maxBlue = 0.0;


    fftw_complex *inRed, *inGreen, *inBlue;
    fftw_complex *outRed, *outGreen, *outBlue;

    inRed = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    inGreen = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    inBlue = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outRed = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outGreen = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outBlue = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);

    QRgb *p = (QRgb*)resultImage->bits();
    for(int i=0; i<size; i++){
        inRed[i][0] = qRed(p[i]);
        inRed[i][1] = 0;
        inGreen[i][0] = qGreen(p[i]);
        inGreen[i][1] = 0;
        inBlue[i][0] = qBlue(p[i]);
        inBlue[i][1] = 0;
    }
    fftw_plan plan;
    plan = fftw_plan_dft_2d(height, width, inRed, outRed, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    plan = fftw_plan_dft_2d(height, width, inGreen, outGreen, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    plan = fftw_plan_dft_2d(height, width, inBlue, outBlue, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    for(int i=0; i<size; i++){
        moduleRed[i] = abs(sqrt(outRed[i][0]*outRed[i][0]+outRed[i][1]*outRed[i][1]));
        moduleRed[i] = log(moduleRed[i]+1.0);
        if(moduleRed[i]>maxRed) maxRed = moduleRed[i];

        moduleGreen[i] = abs(sqrt(outGreen[i][0]*outGreen[i][0]+outGreen[i][1]*outGreen[i][1]));
        moduleGreen[i] = log(moduleGreen[i]+1.0);
        if(moduleGreen[i]>maxGreen) maxGreen = moduleGreen[i];

        moduleBlue[i] = abs(sqrt(outBlue[i][0]*outBlue[i][0]+outBlue[i][1]*outBlue[i][1]));
        moduleBlue[i] = log(moduleBlue[i]+1.0);
        if(moduleBlue[i]>maxBlue) maxBlue = moduleBlue[i];
    }
    p = (QRgb*)resultImage->bits();
    for(int i=0; i<size; i++){
        red = clamp(qRound((moduleRed[i]/maxRed)*255.0),0,255);
        green = clamp(qRound((moduleGreen[i]/maxGreen)*255.0),0,255);
        blue = clamp(qRound((moduleBlue[i]/maxBlue)*255.0),0,255);

        p[i] = qRgb(red, green, blue);
    }
    replaceParts(resultImage);

    fftw_free(inRed);
    fftw_free(outRed);
    fftw_free(inGreen);
    fftw_free(outGreen);
    fftw_free(inBlue);
    fftw_free(outBlue);

}
void FourierTransform::fourierPhase(){
    int width = resultImage->width();
    int height = resultImage->height();
    int size =width*height;
    int red,green,blue;
    double phaseRed[size], phaseGreen[size], phaseBlue[size];
    double max = M_PI/2;

    fftw_complex *inRed, *inGreen, *inBlue;
    fftw_complex *outRed, *outGreen, *outBlue;

    inRed = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    inGreen = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    inBlue = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outRed = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outGreen = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outBlue = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);

    QRgb *p = (QRgb*)resultImage->bits();
    for(int i=0; i<size; i++){
        inRed[i][0] = qRed(p[i]);
        inRed[i][1] = 0;
        inGreen[i][0] = qGreen(p[i]);
        inGreen[i][1] = 0;
        inBlue[i][0] = qBlue(p[i]);
        inBlue[i][1] = 0;
    }
    fftw_plan plan;
    plan = fftw_plan_dft_2d(height, width, inRed, outRed, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    plan = fftw_plan_dft_2d(height, width, inGreen, outGreen, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    plan = fftw_plan_dft_2d(height, width, inBlue, outBlue, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    p = (QRgb*)resultImage->bits();
    for(int i=0; i<size; i++){
        phaseRed[i] = abs(atan(outRed[i][1]/outRed[i][0]));
        phaseRed[i] = (log(phaseRed[i]+1.0)/max)*255.0;
        red = clamp(qRound(phaseRed[i]),0,255);

        phaseGreen[i] = abs(atan(outGreen[i][1]/outGreen[i][0]));
        phaseGreen[i] = (log(phaseGreen[i]+1.0)/max)*255.0;
        green = clamp(qRound(phaseGreen[i]),0,255);

        phaseBlue[i] = abs(atan(outBlue[i][1]/outBlue[i][0]));
        phaseBlue[i] = (log(phaseBlue[i]+1.0)/max)*255.0;
        blue = clamp(qRound(phaseBlue[i]),0,255);

        p[i] = qRgb(red, green, blue);
    }

    replaceParts(resultImage);

    fftw_free(inRed);
    fftw_free(outRed);
    fftw_free(inGreen);
    fftw_free(outGreen);
    fftw_free(inBlue);
    fftw_free(outBlue);

}
void FourierTransform::fourierReal(){
    int width = resultImage->width();
    int height = resultImage->height();
    int size =width*height;
    int red,green,blue;
    double realRed[size], realGreen[size], realBlue[size];
    double maxRed = 0.0;
    double maxGreen = 0.0;
    double maxBlue = 0.0;


    fftw_complex *inRed, *inGreen, *inBlue;
    fftw_complex *outRed, *outGreen, *outBlue;

    inRed = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    inGreen = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    inBlue = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outRed = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outGreen = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outBlue = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);

    QRgb *p = (QRgb*)resultImage->bits();
    for(int i=0; i<size; i++){
        inRed[i][0] = qRed(p[i]);
        inRed[i][1] = 0;
        inGreen[i][0] = qGreen(p[i]);
        inGreen[i][1] = 0;
        inBlue[i][0] = qBlue(p[i]);
        inBlue[i][1] = 0;
    }
    fftw_plan plan;
    plan = fftw_plan_dft_2d(height, width, inRed, outRed, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    plan = fftw_plan_dft_2d(height, width, inGreen, outGreen, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    plan = fftw_plan_dft_2d(height, width, inBlue, outBlue, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    for(int i=0; i<size; i++){
        realRed[i] = abs(outRed[i][0]);
        realRed[i] = log(realRed[i]+1.0);
        if(realRed[i]>maxRed) maxRed = realRed[i];

        realGreen[i] = abs(outGreen[i][0]);
        realGreen[i] = log(realGreen[i]+1.0);
        if(realGreen[i]>maxGreen) maxGreen = realGreen[i];

        realBlue[i] = abs(outBlue[i][0]);
        realBlue[i] = log(realBlue[i]+1.0);
        if(realBlue[i]>maxBlue) maxBlue = realBlue[i];
    }
    p = (QRgb*)resultImage->bits();
    for(int i=0; i<size; i++){
        red = clamp(qRound((realRed[i]/maxRed)*255.0),0,255);
        green = clamp(qRound((realGreen[i]/maxGreen)*255.0),0,255);
        blue = clamp(qRound((realBlue[i]/maxBlue)*255.0),0,255);

        p[i] = qRgb(red, green, blue);
    }
    replaceParts(resultImage);

    fftw_free(inRed);
    fftw_free(outRed);
    fftw_free(inGreen);
    fftw_free(outGreen);
    fftw_free(inBlue);
    fftw_free(outBlue);

}
void FourierTransform::fourierImaginary(){
    int width = resultImage->width();
    int height = resultImage->height();
    int size =width*height;
    int red,green,blue;
    double imRed[size], imGreen[size], imBlue[size];
    double maxRed = 0.0;
    double maxGreen = 0.0;
    double maxBlue = 0.0;


    fftw_complex *inRed, *inGreen, *inBlue;
    fftw_complex *outRed, *outGreen, *outBlue;

    inRed = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    inGreen = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    inBlue = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outRed = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outGreen = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);
    outBlue = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*size);

    QRgb *p = (QRgb*)resultImage->bits();
    for(int i=0; i<size; i++){
        inRed[i][0] = qRed(p[i]);
        inRed[i][1] = 0;
        inGreen[i][0] = qGreen(p[i]);
        inGreen[i][1] = 0;
        inBlue[i][0] = qBlue(p[i]);
        inBlue[i][1] = 0;
    }
    fftw_plan plan;
    plan = fftw_plan_dft_2d(height, width, inRed, outRed, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    plan = fftw_plan_dft_2d(height, width, inGreen, outGreen, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    plan = fftw_plan_dft_2d(height, width, inBlue, outBlue, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);

    for(int i=0; i<size; i++){
        imRed[i] = abs(outRed[i][1]);
        imRed[i] = log(imRed[i]+1.0);
        if(imRed[i]>maxRed) maxRed = imRed[i];

        imGreen[i] = abs(outGreen[i][1]);
        imGreen[i] = log(imGreen[i]+1.0);
        if(imGreen[i]>maxGreen) maxGreen = imGreen[i];

        imBlue[i] = abs(outBlue[i][1]);
        imBlue[i] = log(imBlue[i]+1.0);
        if(imBlue[i]>maxBlue) maxBlue = imBlue[i];
    }
    p = (QRgb*)resultImage->bits();
    for(int i=0; i<size; i++){
        red = clamp(qRound((imRed[i]/maxRed)*255.0),0,255);
        green = clamp(qRound((imGreen[i]/maxGreen)*255.0),0,255);
        blue = clamp(qRound((imBlue[i]/maxBlue)*255.0),0,255);

        p[i] = qRgb(red, green, blue);
    }
    replaceParts(resultImage);

    fftw_free(inRed);
    fftw_free(outRed);
    fftw_free(inGreen);
    fftw_free(outGreen);
    fftw_free(inBlue);
    fftw_free(outBlue);
}
void FourierTransform::okSlot(){
    *resultImage = orginalImage;
    if(module->isChecked()) fourierModule();
    if(phase->isChecked()) fourierPhase();
    if(realis->isChecked()) fourierReal();
    if(imaginalis->isChecked()) fourierImaginary();
    emit updateImageSignal();
}
void FourierTransform::cancel(){
    *resultImage = orginalImage;
    close();
    emit updateImageSignal();
}
