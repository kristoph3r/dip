#ifndef FOURIERTRANSFORM_H
#define FOURIERTRANSFORM_H

#include <imagealgorithms.h>
#include <fftw-3.3.4/api/fftw3.h>
#include <math.h>
#include <QWidget>
#include <QRadioButton>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

class FourierTransform : public QWidget
{
    Q_OBJECT
    QImage *resultImage;
    QImage orginalImage;
    QRadioButton *imageRadio;
    QRadioButton *module;
    QRadioButton *phase;
    QRadioButton *realis;
    QRadioButton *imaginalis;

    QPushButton *okButton;
    QPushButton *cancelButton;

    QVBoxLayout *radioLayout;
    QHBoxLayout *buttonLayout;
    QVBoxLayout *fourierLayout;

    void fourierModule();
    void fourierPhase();
    void fourierReal();
    void fourierImaginary();

public:
    FourierTransform(QWidget *parent = 0);
    void setImage(QImage*);

signals:
    void updateImageSignal();

public slots:
    void okSlot();
    void cancel();
};

#endif // FOURIERTRANSFORM_H
